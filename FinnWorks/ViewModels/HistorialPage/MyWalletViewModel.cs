﻿using FinnWorks.Models.HistorialPage;
using FinnWorks.Services.API;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Model = FinnWorks.Models.HistorialPage.Transaction;

namespace FinnWorks.ViewModels.HistorialPage
{
    /// <summary>
    /// ViewModel for my wallet page.
    /// </summary>
    [Preserve(AllMembers = true)]
    public class MyWalletViewModel : BaseViewModel
    {
        #region Fields

        private int selectedIndex;

        private double _totalPagado,_totalPendiente,_totalSolicitado;

        private string[] months, years, xValues;

        private ObservableCollection<Model> monthListItems;

        private ObservableCollection<Model> yearListItems;

        private ObservableCollection<Model> listItems;

        public ObservableCollection<TransactionChartData> ChartData { get; set; }

        public ObservableCollection<Model> DataSource { get; set; }

        private Command<object> itemTappedCommand;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance for the <see cref="MyWalletViewModel" /> class.
        /// </summary>
        public MyWalletViewModel()
        {
            months = new string[] { "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" };
            years = new string[0];

            ChartData = new ObservableCollection<TransactionChartData>();
            DataSource = new ObservableCollection<Model>()
            {
                new Model(){ Duration = "Mes" },
                new Model(){ Duration = "Año" }
            };
            selectedIndex = 0;
            ListItems = new ObservableCollection<Model>();
            this.LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());
        }

        #endregion

        #region C O M A N D O S
        public Command LoadItemsCommand { get; set; }
        async Task ExecuteLoadItemsCommand()
        {
            IsBusy = true;

            try
            {
                ListItems.Clear();

                var disposicionesService = new DisposicionesService();

                var usuario = await App.Database.GetUsuarioActual();
                var historial = await disposicionesService.GetHistorial(usuario);

                if (historial != null)
                {
                    var FechaI = historial.Min(x => x.FechaDisposicion);
                    var FechaF = historial.Min(x => x.FechaDisposicion);

                    var anios = new List<string>();
                    for (int i = FechaI.Year; i <= FechaF.Year; i++)
                    {
                        anios.Add(i.ToString());
                    }
                    years = anios.ToArray();

                    foreach (var item in historial)
                    {
                        string TipoMovimiento = "";
                        Color labelColor = Color.DarkBlue;
                        Color filaColor = Color.FromHex("#E9EDEF");
                        string profileImage = "hand_blue.png";

                        if (item.CveConcepto == 1)
                        {
                            TipoMovimiento = "+";
                            labelColor = Color.FromHex("#1BBC9B");
                            filaColor = Color.White;
                            profileImage = "money_check.png";
                        }
                        else if (item.CveConcepto == 2)
                        {
                            filaColor = Color.White;
                        }
                        else if (item.CveConcepto == 3)
                        {
                            TipoMovimiento = "-";
                            labelColor = Color.Red;
                            filaColor = Color.White;
                            profileImage = "mastercard.png";
                        }
                        else if (item.CveConcepto == -2)
                        {
                            TipoMovimiento = "-";
                            labelColor = Color.Red;
                            filaColor = Color.White;
                        }

                        ListItems.Add(new Model()
                        {
                            ProfileImage = profileImage,
                            Name = item.Concepto,
                            Title = "",
                            Amount = item.Importe,
                            Date = !item.CveConcepto.HasValue ? item.FechaDisposicion : (item.FechaMovimiento ?? item.FechaDisposicion),
                            TipoMovimiento = TipoMovimiento,
                            ColorImporte = labelColor,
                            ColorFila = filaColor,
                            CveConcepto = item.CveConcepto ?? -1,
                            CveDisposicion = item.CveDisposicion
                        });
                    }
                }

                MonthData();
                YearData();
                UpdateListViewData();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                OnExecuted(EventArgs.Empty);
                IsBusy = false;
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the my wallet items collection in a month.
        /// </summary>
        public ObservableCollection<Model> MonthListItems { get => this.monthListItems; set => SetProperty(ref this.monthListItems, value); }

        /// <summary>
        /// Gets or sets the my wallet items collection in a year.
        /// </summary>
        public ObservableCollection<Model> YearListItems { get => this.yearListItems; set => SetProperty(ref this.yearListItems, value); }

        /// <summary>
        /// Gets or sets the my wallet items collection.
        /// </summary>
        public ObservableCollection<Model> ListItems { get => this.listItems; set => SetProperty(ref this.listItems, value); }


        /// <summary>
        /// Gets the command that will be executed when an item is selected.
        /// </summary>
        public Command<object> ItemTappedCommand
        {
            get
            {
                return this.itemTappedCommand ?? (this.itemTappedCommand = new Command<object>(this.NavigateToNextPage));
            }
        }

        /// <summary>
        /// Gets or sets the selected index of combobox.
        /// </summary>
        public int SelectedIndex
        {
            get
            {
                return selectedIndex;
            }
            set
            {
                SetProperty(ref this.selectedIndex, value);
                UpdateListViewData();
            }
        }

        /// <summary>
        /// Gets or sets the total balance remaining in the wallet.
        /// </summary>
        public double TotalSolicitado { get => _totalSolicitado; set => SetProperty(ref _totalSolicitado, value); }
        public double TotalPagado { get => _totalPagado; set => SetProperty(ref _totalPagado, value); }
        public double TotalPendiente { get => _totalPendiente; set => SetProperty(ref _totalPendiente, value); }
        #endregion

        #region Methods

        /// <summary>
        /// Month data collection.
        /// </summary>
        private void MonthData()
        {
            if (monthListItems == null)
            {
                monthListItems = new ObservableCollection<Model>();
            }
            else
            {
                monthListItems.Clear();
            }

            if (listItems != null)
            {
                DateTime FechaF = DateTime.Now.AddDays(1);
                DateTime FechaI = new DateTime(FechaF.Year, 1, 1);

                var historial = listItems.Where(x => x.Date >= FechaI && x.Date <= FechaF);
                foreach (var item in historial)
                {
                    monthListItems.Add(item);
                }
            }
        }

        /// <summary>
        /// Year data collection.
        /// </summary>
        private void YearData()
        {

            if (yearListItems == null)
            {
                yearListItems = new ObservableCollection<Model>();
            }
            else
            {
                yearListItems.Clear();
            }

            if (listItems != null)
            {
                DateTime FechaF = DateTime.Now.AddDays(1);
                DateTime FechaI = new DateTime(FechaF.Year, 1, 1);

                var historial = listItems.Where(x => x.Date >= FechaI && x.Date <= FechaF);
                foreach (var item in historial)
                {
                    yearListItems.Add(item);
                }
            }
        }

        /// <summary>
        /// Method for update the listview items.
        /// </summary>
        private void UpdateListViewData()
        {
            ObservableCollection<Model> items = null;
            switch (SelectedIndex)
            {
                case 0:
                    items = MonthListItems;
                    xValues = months;
                    UpdateChartData(items, 0);
                    break;
                case 1:
                    items = YearListItems;
                    xValues = years;
                    UpdateChartData(items, 1);
                    break;
                default:
                    break;
            }

        }

        /// <summary>
        /// Method for update the chart data.
        /// </summary>
        private void UpdateChartData(ObservableCollection<Model> items, int index)
        {
            ChartData.Clear();
            TotalPagado = 0;
            TotalSolicitado = 0;
            TotalPendiente = 0;
            if (items != null)
            {
                switch (index)
                {
                    case 0: //MES
                        foreach (var item in xValues)
                        {
                            int indexMonth = xValues.IndexOf(item) + 1;
                            var disposiciones = items.Where(d => d.CveConcepto == 1 && d.Date.Month == indexMonth).Sum(x => x.Amount);
                            var comisiones = items.Where(p => p.CveConcepto == 2 && p.Date.Month == indexMonth).Sum(x => x.Amount);
                            var pagos = items.Where(p => p.CveConcepto == 3 && p.Date.Month == indexMonth).Sum(x => x.Amount);
                            
                            double pendiente = 0;

                            if ((disposiciones + comisiones) > pagos)
                            {
                                pendiente = (disposiciones + comisiones) - pagos;
                            }

                            TotalPagado += pagos;
                            TotalSolicitado += disposiciones;
                            TotalPendiente += pendiente;

                            ChartData.Add(new TransactionChartData(item, pendiente, disposiciones, 4));
                        }
                        break;

                    case 1: //AÑOS
                        foreach (var item in xValues)
                        {
                            int year = Convert.ToInt32(item);

                            var disposiciones = items.Where(d => d.CveConcepto == 1 && d.Date.Year == year).Sum(x => x.Amount);
                            var comisiones = items.Where(p => p.CveConcepto == 2 && p.Date.Year == year).Sum(x => x.Amount);
                            var pagos = items.Where(p => p.CveConcepto == 3 && p.Date.Year == year).Sum(x => x.Amount);

                            double pendiente = 0;

                            if ((disposiciones + comisiones) > pagos)
                            {
                                pendiente = (disposiciones + comisiones) - pagos;
                            }

                            TotalPagado += pagos;
                            TotalSolicitado += disposiciones;
                            TotalPendiente += pendiente;

                            ChartData.Add(new TransactionChartData(item, pendiente, disposiciones, 4));
                        }
                        break;
                }
             
                


            }

        }

        /// <summary>
        /// Invoked when an item is selected from the my wallet page.
        /// </summary>
        /// <param name="selectedItem">Selected item from the list view.</param>
        private void NavigateToNextPage(object selectedItem)
        {
            // Do something
        }

        #endregion
    }
}