﻿using FinnWorks.Models;
using FinnWorks.Services.API;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace FinnWorks.ViewModels.Registro
{
    /// <summary>
    /// ViewModel for sign-up page.
    /// </summary>
    [Preserve(AllMembers = true)]
    public class SignUpPageViewModel : LoginViewModel
    {
        #region Fields

        private string name;

        private string curp;

        private string password;

        private string confirmPassword;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance for the <see cref="SignUpPageViewModel" /> class.
        /// </summary>
        public SignUpPageViewModel()
        {
            this.SignUpCommand = new Command(this.SignUpClicked);
        }

        #endregion

        #region Property

        /// <summary>
        /// Gets or sets the property that bounds with an entry that gets the name from user in the Sign Up page.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (this.name == value)
                {
                    return;
                }
                SetProperty(ref this.name, value);
            }
        }

        public string Curp
        {
            get
            {
                return this.curp;
            }

            set
            {
                if (this.curp == value)
                {
                    return;
                }
                SetProperty(ref this.curp, value);
            }
        }

        /// <summary>
        /// Gets or sets the property that bounds with an entry that gets the password from users in the Sign Up page.
        /// </summary>
        public string Password
        {
            get
            {
                return this.password;
            }

            set
            {
                if (this.password == value)
                {
                    return;
                }

                this.password = value;
                SetProperty(ref this.password, value);
            }
        }

        /// <summary>
        /// Gets or sets the property that bounds with an entry that gets the password confirmation from users in the Sign Up page.
        /// </summary>
        public string ConfirmPassword
        {
            get
            {
                return this.confirmPassword;
            }

            set
            {
                if (this.confirmPassword == value)
                {
                    return;
                }

                this.confirmPassword = value;
                SetProperty(ref this.confirmPassword, value);
            }
        }

        #endregion

        #region Command

        /// <summary>
        /// Gets or sets the command that is executed when the Sign Up button is clicked.
        /// </summary>
        public Command SignUpCommand { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Invoked when the Sign Up button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private async void SignUpClicked(object obj)
        {
            if (IsBusy)
            {
                return;
            }

            IsBusy = true;
            ResponseModel resp = null;

            try
            {
                var objUsuarios = new UsuariosService();
                resp = await objUsuarios.Registrar(Email, Name, Password, Curp);
            }
            catch (System.Exception ex)
            {
                resp = new ResponseModel() { IsSuccess = false, Message = ex.ToString() };
            }

            OnUsuarioSolicitado(new ResponseModelEventArgs(resp));
        }

        #endregion

        #region E V E N T O S
        public delegate void ResponseModelEventHandler(object sender, ResponseModelEventArgs e);

        public event ResponseModelEventHandler UsuarioSolicitado;
        protected virtual void OnUsuarioSolicitado(ResponseModelEventArgs e)
        {
            if (UsuarioSolicitado != null)
                UsuarioSolicitado(this, e);
        }
        #endregion
    }
}