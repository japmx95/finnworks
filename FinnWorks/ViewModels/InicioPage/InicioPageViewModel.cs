﻿using FinnWorks.Entities;
using FinnWorks.Models;
using FinnWorks.Models.Disposiciones;
using FinnWorks.Services.API;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace FinnWorks.ViewModels.InicioPage
{
    /// <summary>
    /// ViewModel for inicio page.
    /// </summary>
    [Preserve(AllMembers = true)]
    public class InicioPageViewModel : BaseViewModel
    {
        #region C A M P O S
        private string _nombreUsuario;
        private string _personal;
        private decimal _saldoAcumulado;
        private decimal _saldoPagado;
        private decimal _saldoRechazado;
        private decimal _saldoDisponible;
        private bool _puedeDisponer;
        private string _textoDisposicion;
        private string _ImagenPerfil;
        #endregion

        #region P R O P I E D A D E S
        public string NombreUsuario { get => _nombreUsuario; set => SetProperty(ref _nombreUsuario, value); }
        public string Personal { get => _personal; set => SetProperty(ref _personal, value); }
        public decimal SaldoAcumulado { get => _saldoAcumulado; set => SetProperty(ref _saldoAcumulado, value); }
        public decimal SaldoPagado { get => _saldoPagado; set => SetProperty(ref _saldoPagado, value); }

        public decimal SaldoRechazado { get => _saldoRechazado; set => SetProperty(ref _saldoRechazado, value); }
        public decimal SaldoDisponible { get => _saldoDisponible; set => SetProperty(ref _saldoDisponible, value); }
        public decimal ImporteDisposicion { get; set; }
        public bool PuedeDisponer { get => _puedeDisponer; set => SetProperty(ref _puedeDisponer, value); }
        public string TextoDisposicion { get => _textoDisposicion; set => SetProperty(ref _textoDisposicion, value); }
        public string ImagenPerfil { get => _ImagenPerfil; set => SetProperty(ref _ImagenPerfil, value); }
        #endregion

        #region C O N S T R U C T O R

        /// <summary>
        /// Initializes a new instance for the <see cref="InicioPageViewModel" /> class.
        /// </summary>
        public InicioPageViewModel()
        {
            this.LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand(), () => !IsBusy);
            this.SolicitarDisposicionCommand = new Command(async () => await ExecuteSolicitarDisposicionCommand(), () => !IsBusy);
        }
        #endregion

        #region E V E N T O S
        public delegate void ResponseModelEventHandler(object sender, ResponseModelEventArgs e);

        public event ResponseModelEventHandler DisposicionSolicitada;
        protected virtual void OnDisposicionSolicitada(ResponseModelEventArgs e)
        {
            if (DisposicionSolicitada != null)
                DisposicionSolicitada(this, e);
        }
        #endregion

        #region C O M A N D O S
        public Command LoadItemsCommand { get; set; }
        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
            {
                return;
            }
            IsBusy = true;

            try
            {
                var disposicionesService = new DisposicionesService();

                var usuario = await App.Database.GetUsuarioActual();
                var edoCuenta = await disposicionesService.GetEstadoCuenta(usuario);
                RecargarSaldos(usuario, edoCuenta);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                OnExecuted(EventArgs.Empty);
                IsBusy = false;
            }
        }
        public Command SolicitarDisposicionCommand { get; set; }
        async Task ExecuteSolicitarDisposicionCommand()
        {
            if (IsBusy)
            {
                return;
            }
            IsBusy = true;
            ResponseModel responseSolicitarDisposicion = null;

            try
            {
                if (ImporteDisposicion > 0)
                {
                    var disposicionesService = new DisposicionesService();

                    var usuario = await App.Database.GetUsuarioActual();
                    await Task.Delay(3000);
                    responseSolicitarDisposicion = await disposicionesService.SolicitarDisposicion(usuario, ImporteDisposicion);
                    if (responseSolicitarDisposicion.IsSuccess)
                    {
                        var edoCuenta = await disposicionesService.GetEstadoCuenta(usuario);

                        RecargarSaldos(usuario, edoCuenta);
                    }
                }
                else
                {
                    responseSolicitarDisposicion = new ResponseModel() { IsSuccess = true, Message = "Debe ingresar un importe mayor a '0.00'" };
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                OnDisposicionSolicitada(new ResponseModelEventArgs(responseSolicitarDisposicion));
                IsBusy = false;
            }
        }
        #endregion

        private void RecargarSaldos(MUsuario usuario, EstadoCuenta edoCuenta) 
        {
            this.NombreUsuario = usuario.Usuario;
            this.Personal = usuario.Personal;

            if (string.IsNullOrEmpty(usuario.ImagenPerfil))
            {
                this.ImagenPerfil = BaseAPIService.URLBasePerfiles + "userImage.png";
            }
            else
            {
                this.ImagenPerfil = BaseAPIService.URLBasePerfiles + usuario.ImagenPerfil;
            }

            this.SaldoAcumulado = edoCuenta.ImporteSolicitado;
            this.SaldoRechazado = edoCuenta.ImporteRechazado;
            this.SaldoDisponible = edoCuenta.ImporteDisponible;
            this.SaldoPagado = edoCuenta.ImportePagado;

            this.PuedeDisponer = this.SaldoDisponible > 0;
            if (this.PuedeDisponer)
            {
                this.TextoDisposicion = "Solicitar Disposición";
            }
            else
            {
                if (edoCuenta.ImporteSolicitado > this.SaldoDisponible)
                {
                    this.TextoDisposicion =  "Disposición en proceso. Tu deposito se verá reflejado en un lapso máximo de 15 minutos";
                }
                else
                {
                    this.TextoDisposicion = "No se puede realizar una disposición por el momento";
                }
            }
            
            this.ImporteDisposicion = 0;
        }
    }
}