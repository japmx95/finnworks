﻿using FinnWorks.Entities;
using FinnWorks.Models;
using FinnWorks.Views;
using FinnWorks.Views.LoginPage;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace FinnWorks.ViewModels
{
    /// <summary>
    /// ViewModel for on-boarding gradient page with animation.
    /// </summary>
    [Preserve(AllMembers = true)]
    public class OnBoardingAnimationViewModel : BaseViewModel
    {
        #region Fields

        public MUsuario Usuario { get; set; }

        private ObservableCollection<Boarding> boardings2;

        private string nextButtonText = "Siguiente";

        private bool isSkipButtonVisible = true;

        private int _selectedIndex2;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance for the <see cref="OnBoardingAnimationViewModel" /> class.
        /// </summary>
        public OnBoardingAnimationViewModel(MUsuario usuario)
        {
            this.Usuario = usuario;
            this.SalirCommand = new Command(this.Salir);
            this.NextCommand = new Command(this.Next);

            string tc1 = "\"Aplicación\": Plataforma móvil propiedad de GRUPO PROMOTOR DE OPERACIONES GPO, incluyendo la infraestructura de cómputo, redes de telecomunicaciones, sistemas operativos, bases de datos y software que ocupa para sus operaciones y prestación de los Servicios en favor de los Usuarios, disponible para iOS y Android." +
"\"Costo del Servicio” o “Comisión\": Cantidad establecida y notificada por GRUPO PROMOTOR DE OPERACIONES GPO a los Usuarios por la prestación de los Servicios de retiro de los Montos Retirados, la cual se cobrará por GRUPO PROMOTOR DE OPERACIONES GPO bajo concepto de comisión, cuyo monto se señala en la Aplicación." +
"\"Convenio de Colaboración\": Instrumento legal celebrado entre GRUPO PROMOTOR DE OPERACIONES GPO y la Empresa Afiliada para la cual labora o presta sus servicios el Usuario, el cual tiene como objeto señalar las actividades a realizarse por GRUPO PROMOTOR DE OPERACIONES GPO y la Empresa Afiliada que corresponda para permitir la prestación de los Servicios en favor del Usuario." +
"\"Cuenta\": Registro del Usuario en la Aplicación que le permite utilizar la misma, la cual será de uso exclusivo del Usuario, y cuyos datos de acceso como lo son nombre de usuario o contraseña no deberá compartir con terceros." +
"\"Cuenta Ordenante\": Cuenta bancaria de depósito a la vista, incluida la cuenta básica de nómina, o cualquier otra cuenta de depósito o cheques permitida por la Aplicación, abierta con una Entidad Financiera a nombre del Usuario de que se trate, en la que se pueden realizar, entre otros, abonos de los Recursos derivado de prestaciones laborales o de prestación de servicios. Lo anterior, bajo el entendido que la Cuenta Ordenante debe ser forzosamente la misma cuenta en la que la Empresa Afiliada para la cual labora o presta sus servicios el Usuario realiza el pago de su sueldo, contraprestación y/o compensación, según sea el caso." +
"\"Disponible para Retiro\": El monto total que puede retirar el Usuario en cuestión, ya sea en una o varias disposiciones, limitado a un monto máximo determinado entre GRUPO PROMOTOR DE OPERACIONES GPO y la Empresa Afiliada." +
"\"Entidad Financiera\": Cualquier sociedad o institución que preste servicios financieros, constituida, existente y debidamente autorizada para tales efectos en términos de la legislación aplicable, que en específico preste servicios financieros para operaciones pasivas relacionadas a la apertura de una Cuenta Ordenante, la cual deberá estar regulada y supervisada por las autoridades financieras competentes." +
"\"Empresa Afiliada\": La sociedad/empresa que de forma previa a que el Usuario pueda aceptar estos Términos y Condiciones, tenga celebrado: (i) un Convenio de Colaboración con GRUPO PROMOTOR DE OPERACIONES GPO ; y (ii) un contrato individual de trabajo o de prestación de servicios o cualquier contrato similar o de distinta naturaleza con el Usuario de que se trate, la cual lleva a cabo el depósito a la Cuenta Ordenante del Usuario, derivado de los salarios, honorarios, comisiones, y/o cualquier otro tipo de contraprestación acordada entre el Usuario y la sociedad/empresa de que se trate." +
"Lo anterior, en el entendido que la presente definición aplica exclusivamente para efectos de regular la prestación de los Servicios a través de la Aplicación, sin que ello implique o genere un vínculo legal entre la Empresa Afiliada y/o el Usuario, a menos que así lo acuerden en documentos independientes a los presentes términos y condiciones, circunstancia que será completamente ajena a GRUPO PROMOTOR DE OPERACIONES GPO." +
"GRUPO PROMOTOR DE OPERACIONES GPO: Grupo Promotor de Operaciones GPO, S.A. de C.V." +
"Montos Retirados: El o los importe(s) en dinero depositado o transferido por GRUPO PROMOTOR DE OPERACIONES GPO a solicitud del Usuario a través de la Aplicación de tiempo en tiempo, a la Cuenta Ordenante en virtud de la prestación de los Servicios a través de la Aplicación y derivado de la retribución que se paga al Usuario." +
"Recursos: El importe en dinero depositado o transferido a la Cuenta Ordenante del Usuario por parte de la Empresa Afiliada, derivado de los salarios, honorarios, comisiones, y/o cualquier otro tipo de contraprestación acordada entre el Usuario y la Empresa Afiliada por el trabajo, actividades y/o la prestación de los servicios del Usuario para la Empresa Afiliada." +
"Servicios: Los descritos en el apartado de Servicios de la Aplicación en los presentes términos y condiciones, y que son prestados por GRUPO PROMOTOR DE OPERACIONES GPO en favor de los Usuarios cuyas Empresas Afiliadas tengan celebrado un Convenio de Colaboración con GRUPO PROMOTOR DE OPERACIONES GPO." +
"Usuario: Cualquier persona física que labora o presta sus servicios para una Empresa Afiliada que firmó un Convenio de Colaboración con GRUPO PROMOTOR DE OPERACIONES GPO, y que desea recibir cualquiera de los Servicios que GRUPO PROMOTOR DE OPERACIONES GPO ofrece a través de dicha Aplicación, para lo cual deberá bajar la Aplicación en su dispositivo de comunicación móvil.";


            string tc2 = "Estos Términos y Condiciones generales de uso son aplicables al acceso, uso o ingreso a la Aplicación propiedad de GRUPO PROMOTOR DE OPERACIONES,GPO así como a los Servicios proporcionados por GRUPO PROMOTOR DE OPERACIONES GPO a cualquier Usuario, salvo en los casos que expresamente se indique lo contrario dentro de los presentes Términos y Condiciones." +
"Al ingresar y utilizar la Aplicación y/o abrir una Cuenta, el Usuario, acepta automática y expresamente los presentes Términos y Condiciones generales de uso." +
"El Usuario declara ser mayor de edad y tener capacidad de goce y ejercicio contando, con plena capacidad para contratar y contraer toda clase de obligaciones y derechos de los presentes términos y condiciones, así como capacidad para cumplir cabal y puntualmente dichas obligaciones." +
"El Usuario acepta y reconoce expresamente que la aceptación de estos Términos y Condiciones generales de uso y/o el uso de la Aplicación por parte del Usuario, no crean un contrato de sociedad, de mandato, de franquicia y/o relación laboral entre GRUPO PROMOTOR DE OPERACIONES GPO y el Usuario." +
"Si el Usuario no está de acuerdo en aceptar en forma absoluta, incondicional y completa estos Términos y Condiciones deberá, abstenerse de usar la Aplicación y solicitar o abrir una Cuenta en la Aplicación y/o utilizar los Servicios ofrecidos por GRUPO PROMOTOR DE OPERACIONES GPO. En caso contrario, se considerarán expresamente aceptados los presentes Términos y Condiciones generales de uso." +
"Para todo lo no expresamente detallado en los presentes términos y condiciones, se estará a lo dispuesto de forma explícita en el Convenio de Colaboración celebrado con la Empresa Afiliada para la cual labore el Usuario, y en caso de que exista controversia entre lo dispuesto por los presentes Términos y Condiciones y el Convenio de Colaboración celebrado con la Empresa Afiliada que corresponda, prevalecerá lo dispuesto por los presentes Términos y Condiciones.";

            string tc3 = "Al ingresar a la Aplicación, el Usuario reconoce que GRUPO PROMOTOR DE OPERACIONES GPO le está otorgando una licencia limitada, temporal y revocable en los términos que a continuación se describen." +
"El Usuario está de acuerdo que el uso de la Aplicación por su parte se realiza bajo su propia responsabilidad, en relación con los Servicios y/o productos que se prestan." +
"Toda la información suministrada por el Usuario al usar la Aplicación y cualquier otro dato e información que contenga sus datos personales, se usará de acuerdo al Aviso de Privacidad de GRUPO PROMOTOR DE OPERACIONES GPO, mismo que se pone a disposición del Usuario por medio de la Aplicación y que se encuentra disponible en la Página Web.";

            string tc4 = "Para poder hacer uso de los Servicios de la Aplicación, el Usuario deberá previamente completar el formulario de registro puesto a disposición por GRUPO PROMOTOR DE OPERACIONES GPO, en el cual se requieren sus datos personales, los cuales enunciativa más no limitativamente pueden ser: (i) nombre completo; (ii) correo electrónico con dominio activo de la Empresa Afiliada y validado por ésta misma; y (iii) número telefónico, en caso de no contar con el correo electrónico. El Usuario se obliga expresamente a proporcionar información correcta y veraz, así como a no proporcionar datos e información de terceros, para los efectos previstos en el presente párrafo." +
"Posteriormente se enviará al correo registrado por el Usuario o, en su caso, al teléfono celular registrado por el Usuario, un código de confirmación que deberá ser ingresado por el Usuario en la Aplicación, para que éste a su vez le genere una contraseña provisional que servirá para acceder a la Aplicación. Una vez completado lo anterior, el Usuario deberá generar una nueva contraseña en la Aplicación que servirá para posteriores accesos a la Aplicación. El Usuario reconoce expresamente que el será el único responsable por el resguardo y custodia de su contraseña para evitar que se preste un Servicio que no haya solicitado así como para evitar el mal uso de la contraseña y/o de la Aplicación por parte de terceros que por algún motivo pudieran llegar tener acceso a la contraseña del Usuario, por lo que el Usuario igualmente reconoce expresamente que GRUPO PROMOTOR DE OPERACIONES GPO no será responsable de lo anterior y deberá indemnizar y sacar en paz y a salvo a GRUPO PROMOTOR DE OPERACIONES GPO del mal uso de la contraseña y/o de la Aplicación tanto de parte del Usuario y/o por parte de terceros." +
"Todos los datos personales que el Usuario proporciona por motivos de registro a través de la Aplicación serán protegidos y manejados por GRUPO PROMOTOR DE OPERACIONES GPO conforme al Aviso de Privacidad de GRUPO PROMOTOR DE OPERACIONES GPO en apego a la Ley Federal de Protección de Datos Personales en Posesión de los Particulares.";


            string tc5 = "GRUPO PROMOTOR DE OPERACIONES, a través del Convenio de Colaboración celebrado con la Empresa Afiliada, pondrá a disposición del Usuario varias herramientas y funciones a través de su Cuenta en la Aplicación que le permitirán acceder a los Servicios, así como a los Montos Retirados que correspondan. El Usuario podrá solicitar el retiro exclusivamente de la suma de los Montos Retirados que hayan sido solicitados por el Usuario a través de la Aplicación conforme a los presentes términos y condiciones, sujeto a los términos, condiciones y limitaciones previstas dentro del Convenio de Colaboración celebrado entre GRUPO PROMOTOR DE OPERACIONES GPO y la Empresa Afiliada con la cual trabaje, colabore, labore o preste sus servicios el Usuario de que se trate." +
"Los Usuarios de la Aplicación aceptan expresa e incondicionalmente que la prestación de los Servicios no solo se realizará conforme lo establecido por los presentes Términos y Condiciones de Servicio, sino que igualmente se encontrará supeditada a lo dispuesto por el Convenio de Colaboración celebrado por GRUPO PROMOTOR DE OPERACIONES GPO con cada una de las Empresas Afiliadas para la cual labore el Usuario correspondiente." +
"GRUPO PROMOTOR DE OPERACIONES GPO, bajo ninguna circunstancia tendrá la facultad u obligación de prestar el servicio a los Usuarios que no estén aprobados por el área de recursos humanos de la Empresa Afiliada a la cual pertenezca/corresponda. Por su parte, la Empresa Afiliada podrá solicitar, en cualquier momento, la cancelación o bloqueo de los Usuarios que trabajen, laboren, colaboren o presten sus servicios con dicha Empresa Afiliada, para efecto de que el Usuario de que se trate ya no se le preste el Servicio ni se le permita el acceso a su Cuenta." +
"El Usuario que solicite los Servicios de GRUPO PROMOTOR DE OPERACIONES GPO y sea candidato por parte de la Empresa Afiliada para recibir los Montos Retirados, podrá solicitarlos a través de la Aplicación, única y exclusivamente respecto del saldo que tenga efectivamente acumulado/devengado pero NO pagado aun por la Empresa Afiliada. El Usuario autorizará e instruirá a la Empresa Afiliada a depositar en su nombre y a su cuenta a favor de GRUPO PROMOTOR DE OPERACIONES GPO la suma del total de los Recursos que solicitó a través de la Aplicación, así como la cantidad del Costo del Servicio, lo cual será realizado por la Empresa Afiliada a cuenta de los recursos económicos pagaderos por la Empresa Afiliada al Usuario en términos del Convenio de Colaboración celebrado con GRUPO PROMOTOR DE OPERACIONES GPO." +
"El porcentaje límite que el Usuario podrá retirar, así como el número máximo de retiros que podrán hacer por periodo de pago a través de la Aplicación, se determinará mediante acuerdo entre GRUPO PROMOTOR DE OPERACIONES GPO y la Empresa Afiliada, el cual se plasmará en el Convenio de Colaboración correspondiente." +
"El Usuario reconoce expresamente que los depósitos que reciba por parte de GRUPO PROMOTOR DE OPERACIONES GPO por concepto de Montos Retirados de conformidad con la solicitud realizada por el Usuario través de la Aplicación, no deberán interpretarse como la existencia y/o nacimiento de una relación laboral entre GRUPO PROMOTOR DE OPERACIONES GPO y el Usuario o de cualquier otra índole distinta a la expresamente prevista dentro de los presentes términos y condiciones, toda vez que su único patrón es la Empresa Afiliada, quien es la responsable de cubrir todas las prestaciones y conceptos derivados de la relación que los une a la Empresa Afiliada y al Usuario para todos los efectos legales, por lo cual el Usuario acepta expresamente que no podrá realizar reclamaciones en contra de GRUPO PROMOTOR DE OPERACIONES GPO argumentando una obligación a cargo de GRUPO PROMOTOR DE OPERACIONES GPO en favor del Usuario de pagar sueldos y/o contraprestaciones de cualquier naturaleza, ni alegando la existencia de una relación laboral entre GRUPO PROMOTOR DE OPERACIONES GPOy el Usuario." +
"GRUPO PROMOTOR DE OPERACIONES GPO y la Empresa Afiliada podrán limitar inicialmente los retiros por pago de salario y/o prestación de servicios que puede solicitar el Usuario, según el esquema de pago que el Usuario y la Empresa Afiliada manejen y acuerden al amparo del convenio o acuerdo que al efecto hubieren suscrito. De igual manera, el Usuario reconoce expresamente que GRUPO PROMOTOR DE OPERACIONES GPO, sin previo aviso, podrá limitar el monto máximo de los retiros, por instrucciones de la Empresa Afiliada.";

            string tc6 = "Por cada entrega por parte de GRUPO PROMOTOR DE OPERACIONES GPO al Usuario de Montos Retirados, El Usiario deberá pagar a GRUPO PROMOTOR DE OPERACIONES GPO la cuota por retiro que quede establecida en la aplicación, en el momento de realzar la disposición del monto que GRUPO PROMOTOR DE OPERACIONES deposite en la Cuenta Ordenante del Usuario. Lo anterior se efectuará a través de las instrucciones irrevocables de pago que al efecto se girarán a través de la Aplicación a nombre del Usuario y dirigidas a la Empresa Afiliada en la cual labora o presta sus servicios, cada vez que el Usuario solicite disposición de Montos Retirados GRUPO PROMOTOR DE OPERACIONES GPO solicitara a la empresa afiliada que corresponda realizar la retención y posteriormente el deposito del reembolso por las cantidades que el Usuario hubiere dispuesto a través de la Aplicación como Montos Retirados, así como el pago de las comisiones correspondientes que hubieren resultado por el número de disposiciones de los Montos Retirados realizados por el Usuario durante el periodo correspondiente." +
"Los reembolsos y pagos a realizarse por la Empresa Afiliada en favor de GRUPO PROMOTOR DE OPERACIONES GPO de conformidad con lo dispuesto por el párrafo inmediato anterior, los realizará la Empresa Afiliada en nombre, representación y a cuenta del Usuario que corresponda, haciendo uso de los montos originalmente pagaderos por la Empresa Afiliada al Usuario con motivo de la relación laboral o comercial existente entre el Usuario y la Empresa Afiliada. En dicho tenor, una vez realizados los reembolsos y pagos por parte de la Empresa Afiliada, pagará al Usuario los montos remanentes en el periodo de pago de nómina o pago de contraprestación que corresponda." +
"La comisión por los Servicios podrá ser pagada por la Empresa Afiliada para la cual labora o presta sus servicios el Usuario, en caso de que la Empresa Afiliada correspondiente así lo haya pactado en el Convenio de Colaboración que hubiere suscrito con GRUPO PROMOTOR DE OPERACIONES GPO, en cuyo caso el Usuario no estará obligado a pagar a GRUPO PROMOTOR DE OPERACIONES GPO la comisión por los Servicios, y únicamente girará instrucción a la Empresa Afiliada a través de la Aplicación, cada vez que el Usuario solicite Montos Retirados a través de la Aplicación." +
"Asimismo y de forma indistinta, GRUPO PROMOTOR DE OPERACIONES GPO podrá cobrar a los Usuarios las cantidades de las que los Usuarios hubieren dispuesto a través de la Aplicación como Montos Retirados, así como las comisiones correspondientes que hubieren resultado por el número de disposiciones de los Montos Retirados realizados por el Usuario durante el periodo correspondiente, así como daños y perjuicios, mediante otras formas de cobro y pago, como lo son de manera enunciativa y no limitativa, cobro directo al Usuario, pagos domiciliados o cargos recurrentes a tarjetas de crédito, débito o nómina del Usuario (incluyendo la Cuenta Ordenante), CoDi (Cobro Digital BANXICO), u otros medios que GRUPO PROMOTOR DE OPERACIONES GPO estime convenientes, sin perjuicio de cualesquier otros medios previstos por la legislación aplicable.";

            string tc7 = "El Usuario se obliga a informar de inmediato a la Empresa Afiliada en donde colabore sobre cualquier actualización de su información de registro en la Aplicación, incluyendo sin limitación, cambios en la Cuenta Ordenante, lo cual deberá realizarse dentro de las 24 (veinticuatro) horas siguientes a la fecha en que se lleve a cabo el cambio." +
"Derivado de lo anterior, GRUPO PROMOTOR DE OPERACIONES GPO no será responsable de ningún error en el proceso de retiro u otros problemas relacionados con la Aplicación que surjan por la omisión por parte del Usuario o falta de cumplimiento a lo anterior.";

            string tc8 = "GRUPO PROMOTOR DE OPERACIONES GPO y su Aplicación, sus logotipos y todo el material que aparece en la Página Web incluyendo de manera enunciativa más no limitativa: marcas, nombres de dominio y/o nombres comerciales propiedad de sus respectivos titulares protegidos por los tratados internacionales y leyes federales, locales o internacionales aplicables en materia de propiedad industrial y derechos de autor." +
"Los derechos de autor sobre el contenido, organización, recopilación, compilación, información, logotipos, programas, aplicaciones, o en general cualquier información contenida o publicada en la aplicación se encuentran debidamente protegidos a favor de GRUPO PROMOTOR DE OPERACIONES GPO, sus afiliadas, empresas relacionadas o controladoras, aliados estratégicos, proveedores y/o de sus respectivos propietarios, de conformidad con la legislación aplicable en materia de propiedad intelectual o cualesquier otras leyes aplicables." +
"Se prohíbe expresamente al Usuario usar, explotar, modificar, reproducir, alterar, editar o suprimir, ya sea en forma total o parcial, los avisos, marcas, nombres comerciales, señas, anuncios, logotipos o en general cualquier indicación o información contenida en la Aplicación." +
"En caso de que el Usuario transmita a GRUPO PROMOTOR DE OPERACIONES GPO cualquier información, datos, programas, aplicaciones, software o en general cualquier material que requiera ser licenciado a través de la Aplicación, el Usuario le otorga a GRUPO PROMOTOR DE OPERACIONES GPO en este acto una licencia perpetua, irrevocable, incondicional, universal, gratuita, no exclusiva, mundial y libre de regalías, incluyendo entre los derechos otorgados, el derecho de sublicenciar, vender, reproducir, distribuir, transmitir, crear trabajos derivados, exhibirlos y ejecutarlos públicamente." +
"Lo establecido en el párrafo anterior se aplicará igualmente a cualquier otra información que el Usuario envíe o transmita a GRUPO PROMOTOR DE OPERACIONES GPO, incluyendo, sin limitación alguna, ideas para renovar o mejorar la Aplicación, sea que éstas hayan sido incluidas en cualquier espacio de la Aplicación en virtud de otros medios o modos de transmisión ya conocidos o en el futuro desarrollados." +
"El Usuario renuncia expresamente en este acto a intentar cualquier acción, demanda o reclamación en contra de GRUPO PROMOTOR DE OPERACIONES GPO, sus afiliados o proveedores por cualquier actual o eventual violación de cualquier derecho de autor o propiedad intelectual derivado de la información, programas, aplicaciones, software, ideas y demás material que el propio Usuario envíe sobre la Aplicación.";

            string tc9 = "GRUPO PROMOTOR DE OPERACIONES podrá en cualquier momento y cuando lo considere conveniente, sin necesidad de aviso previo o posterior al Usuario, realizar correcciones, adiciones, mejoras o modificaciones al contenido, presentación, información, servicios, áreas, bases de datos y demás elementos de la Aplicación sin que ello dé lugar ni derecho a ninguna reclamación o indemnización, sin que esto implique reconocimiento de responsabilidad alguna a favor del Usuario. Siendo exclusiva responsabilidad del Usuario asegurarse de tomar conocimiento de tales modificaciones." +
"GRUPO PROMOTOR DE OPERACIONES GPO se reserva el derecho de suprimir o modificar el contenido de la Aplicación, asimismo, no será responsable por cualquier falla o retraso que se genere al eliminar tal material. Bajo ninguna circunstancia GRUPO PROMOTOR DE OPERACIONES GPO será responsable de cualquier daño y/o perjuicio, directo o indirecto, causado en virtud de la confianza del Usuario en información obtenida a través de esta Aplicación." +
"GRUPO PROMOTOR DE OPERACIONES GPO no garantiza que la Aplicación satisfaga la totalidad de los requerimientos del Usuario, o que los Servicios de la Aplicación se mantengan siempre ininterrumpidos, en tiempo, seguros o libres de errores. GRUPO PROMOTOR DE OPERACIONES GPO no garantiza un uso idóneo de la Aplicación por parte del Usuario para fines distintos a los expresamente señalados dentro de los presentes términos y condiciones";


            string tc10 = "GRUPO PROMOTOR DE OPERACIONES GPO no se responsabiliza por cualquier daño, perjuicio o pérdida que sufra un Usuario causado por fallas en el sistema, en el servidor o en Internet. Tampoco será responsable por cualquier virus que pudiera infectar el equipo del Usuario como consecuencia del acceso, uso o examen de la Aplicación o a raíz de cualquier transferencia de datos, archivos, imágenes, textos, o audio contenidos en el mismo. Los Usuarios no podrán imputarle responsabilidad alguna ni exigirle indemnizaciones, en virtud de perjuicios resultantes de dificultades técnicas o fallas en los sistemas o en Internet. GRUPO PROMOTOR DE OPERACIONES GPO no garantiza el acceso y uso continuado o ininterrumpido de su Aplicación y Servicios. El sistema y/o la Aplicación puede eventualmente no estar disponible debido a actualizaciones, dificultades técnicas o fallas de la red, o por cualquier otra circunstancia ajena a GRUPO PROMOTOR DE OPERACIONES GPO; en tales casos se procurará restablecerlo con la mayor celeridad posible, sin que por ello pueda imputársele algún tipo de responsabilidad a GRUPO PROMOTOR DE OPERACIONES GPO ni a sus empresas afiliadas, colaboradoras o partes relacionadas.";

            string tc11 = "El Usuario reconoce expresamente que GRUPO PROMOTOR DE OPERACIONES GPO le ha advertido que existen varias estafas que utilizan la red para realizar transacciones ilícitas como: Smishing, Phishing, Pharming, etc. Por esta razón, es responsabilidad de cada Usuario implementar las medidas correspondientes para evitar ser víctima de cualquiera de estos delitos.";

            string tc12 = "El Usuario deberá leer atenta y comprensivamente los presentes Términos y Condiciones cada vez que acceda a la Aplicación, ya que pueden sufrir modificaciones. GRUPO PROMOTOR DE OPERACIONES GPO se reserva el derecho de modificar los Términos y Condiciones Generales de Uso de este acuerdo de voluntades en cualquier tiempo, siendo efectivas dichas modificaciones de forma inmediata." +
"GRUPO PROMOTOR DE OPERACIONES GPO podrá informar al Usuario cuando haya modificaciones en los Términos y Condiciones a través de la Aplicación, pero será en todo momento obligación del Usuario el consultar los presentes Términos y Condiciones en la página www.migpo.com antes de solicitar a través de la Aplicación Montos Retirados, y hará constar su aceptación a los Términos y Condiciones que se encuentren vigentes en la Página Web al momento de realizar solicitudes de Montos Retirados." +
"Si el Usuario no entiende todos los términos o conceptos dentro de la Página Web, debe consultar con un abogado antes de utilizar la Aplicación o cualquiera de los Servicios que proporciona GRUPO PROMOTOR DE OPERACIONES GPO, y si no puede revisarlos periódicamente para determinar si alguno de los términos ha cambiado, el Usuario deberá abstenerse de utilizar la Aplicación, y asume toda la responsabilidad por el hecho de no hacerlo y acepta que dicho incumplimiento equivale a su renuncia expresa de su derecho a iniciar cualquier acción que tenga como propósito aducir que los términos no han sido modificados o acción respecto de cualquier inconformidad con los presentes Términos y Condiciones (y sus versiones actualizadas),Si el Usuario no entiende todos los términos o conceptos dentro de la Página Web, debe consultar con un abogado antes de utilizar la Aplicación o cualquiera de los Servicios que proporciona Montos Retirados, y si no puede revisarlos periódicamente para determinar si alguno de los términos ha cambiado, el Usuario deberá abstenerse de utilizar la Aplicación, y asume toda la responsabilidad por el hecho de no hacerlo y acepta que dicho incumplimiento equivale a su renuncia expresa de su derecho a iniciar cualquier acción que tenga como propósito aducir que los términos no han sido modificados o acción respecto de cualquier inconformidad con los presentes Términos y Condiciones (y sus versiones actualizadas), y otorga a GRUPO PROMOTOR DE OPERACIONES el finiquito más amplio y libera a GRUPO PROMOTOR DE OPERACIONES de cualquier responsabilidad." +
"De tiempo en tiempo, según lo estime conveniente GRUPO PROMOTOR DE OPERACIONES GPO, éste último podrá adicionar a los términos y condiciones, provisiones adicionales los cuales serán publicados en las áreas específicas o nuevos servicios de la aplicación para su lectura y aceptación. El Usuario reconoce y acepta que dichos términos adicionales formarán parte integrante de los presentes Términos y Condiciones para todos los efectos legales a que haya lugar." +
"Cualquier versión modificada o actualizada de los presentes Términos y Condiciones se entenderá como que suple a todas las versiones precedentes." +
"Si en cualquier momento el Usuario no estuviere de acuerdo con los presentes términos y condiciones, deberá de dejar de utilizar de forma inmediata la Aplicación y los Servicios." +
"Los Términos y Condiciones válidos y vinculantes para el Usuario serán lo más recientes que se hubieren publicado por GRUPO PROMOTOR DE OPERACIONES GPO en la Página Web, quedando sin efectos cualesquier otras versiones de los Términos y Condiciones, ya sean digitales, impresas o firmadas.";

            string tc13 = "El Usuario autoriza a GRUPO PROMOTOR DE OPERACIONES GPO, para que directamente o a través de terceros, pueda hacer cualquier pregunta o investigación que considere necesaria para validar y/o autenticar su identidad e información para la utilización de los servicios y contenidos. Esto puede incluir, pero no está limitado a información y/o documentación acerca del uso de los servicios, o requerir que tome acciones para confirmar dirección de correo electrónico, número de teléfono celular/móvil y la verificación de la información en bases de datos de terceros o por medio de otras fuentes. Este proceso es para fines de verificación interna." +
"GRUPO PROMOTOR DE OPERACIONES GPO se reserva asimismo el derecho de rechazar, suspender, temporal o definitivamente el acceso a la Aplicación, sin necesidad de expresar causa alguna al Usuario. En caso de detectar incongruencias o inconsistencias en la información provista por un Usuario o en caso de detectar actividades sospechosas, GRUPO PROMOTOR DE OPERACIONES GPO automáticamente rechazará, cancelará o suspenderá el acceso a la Aplicación. En ambos casos la decisión de GRUPO PROMOTOR DE OPERACIONES GPO no generará para el Usuario derechos de indemnización o resarcimiento por ningún concepto.";

            string tc14 = "Cada Usuario será responsable por todas las obligaciones y cargas impositivas que correspondan por sus operaciones en la Aplicación sin que pueda imputársele a GRUPO PROMOTOR DE OPERACIONES GPO ningún tipo de responsabilidad derivada de los incumplimientos de los Usuarios. GRUPO PROMOTOR DE OPERACIONES GPO no se responsabiliza por el efectivo cumplimiento de las obligaciones fiscales o impositivas establecidas por la ley vigente y/o cualquier otra obligación que surja en virtud de lo celebrado entre los Usuarios.";

            string tc15 = "La prestación del Servicio y el uso de la Aplicación y de los demás contenidos y servicios tienen, en principio, una duración indefinida. No obstante, GRUPO PROMOTOR DE OPERACIONES GPO está autorizada para dar por terminada o suspender la prestación de Servicios y el uso de la Aplicación a su exclusivo arbitrio en cualquier momento sin responsabilidad alguna. Cuando sea razonablemente posible, GRUPO PROMOTOR DE OPERACIONES GPO comunicará previamente la terminación o suspensión de la prestación de Servicios o Acceso a la aplicación a través de la Aplicación. Dicha suspensión y/o terminación de ninguna manera podrá originar el derecho al Usuario a reclamar a GRUPO PROMOTOR DE OPERACIONES GPO daños y/o perjuicios que pudieran producirle la suspensión y/o terminación de los Servicios o acceso a la Aplicación.";

            string tc16 = "El Usuario es el único responsable de asegurarse que el uso, que dé a los Servicios y la Aplicación sean de conformidad con las leyes, disposiciones, circulares, financieras aplicables. Al utilizar los servicios el Usuario manifiesta y garantiza que no figura en ninguna lista emitidas por organismos nacionales e internacionales en la que se incluya a personas bloqueadas o con restricciones para realizar operaciones financieras. En caso de que se detecte que el Usuario se encuentra en una de esas listas, GRUPO PROMOTOR DE OPERACIONES GPO podrá cancelar a su discreción el Servicio y/o acceso o uso de la Aplicación, y dará aviso al Usuario para los efectos legales correspondientes.";

            string tc17 = "GRUPO PROMOTOR DE OPERACIONES GPO tampoco será responsable cuando en el caso de que el Usuario haya solicitado con otro banco la portabilidad de nómina el depósito no sea traspasado a la cuenta destino, por no ser reconocido por la institución de crédito como concepto de nómina.";

            string tc18 = "Todos los pagos que reciba un Usuario por parte de GRUPO PROMOTOR DE OPERACIONES GPO se entenderán realizados por éste último en nombre y representación de la Empresa Afiliada para la que trabaja el Usuario, por lo que hace al cumplimiento de la obligación de pago a cargo de dicha Empresa Afiliada de salarios, comisiones, contraprestaciones, honorarios, pagos, aguinaldos, primas, fondos de ahorro u otras prestaciones, según sea el caso en concreto." +
"En caso de que la Empresa Afiliada solicite a GRUPO PROMOTOR DE OPERACIONES GPO que realice el pago de alguno de los conceptos referidos en el párrafo inmediato anterior, GRUPO PROMOTOR DE OPERACIONES GPO podrá hacerlo incluso sin que medie solicitud por parte de los Usuarios a través de la Aplicación." +
"Cada que GRUPO PROMOTOR DE OPERACIONES GPO realice un pago a los Usuarios, se entenderá que la obligación de pago a cargo de la Empresa Afiliada frente al Usuario correspondiente (por lo que hace a los conceptos referidos en el primer párrafo de la presente sección XIX) ha quedado cabalmente cumplida a satisfacción del Usuario en cuestión, por lo que la Empresa Afiliada dejará de estar obligada a pagar al Usuario la cantidad que ya recibió anticipadamente de GRUPO PROMOTOR DE OPERACIONES GPO a cuenta de los referidos conceptos.";

            string tc19 = "El Usuario no podrá ceder o transferir cualquiera de sus derechos u obligaciones respecto de estos términos y condiciones, ya sea de forma total o parcial. El Usuario no podrá directa o indirectamente acordar, asignar, ceder ni transferir a un tercero cualquier reclamación en contra de GRUPO PROMOTOR DE OPERACIONES GPO como consecuencia de o relacionado con estos términos y condiciones, la prestación de los Servicios y/o el uso de la Aplicación.";

            string tc20 = "En caso de que se presente una controversia que se derive de los presentes términos y condiciones, de la prestación de los Servicios y/o del acceso a la Aplicación o su uso, o bien se interpreten como un contrato o se relacione con él, GRUPO PROMOTOR DE OPERACIONES GPO y el Usuario se someten expresamente a la legislación aplicable de la Ciudad de Hermosillo, Sonora, así como a la jurisdicción de los tribunales competentes los Tribunales de la Ciudad de Hermosillo, Sonora, renunciando a cualquier otro fuero que pudiera corresponderles a razón de sus domicilios presentes o futuros o por cualquier otro motivo.";

            string tc21 = "La información publicada en la Aplicación no necesariamente refleja la posición de GRUPO PROMOTOR DE OPERACIONES GPO ni de sus empleados, oficiales, directores, accionistas, licenciatarios y concesionarios. Por esta razón, GRUPO PROMOTOR DE OPERACIONES GPO no se hace responsable por ninguna información, y conceptos que se emitan en la Aplicación." +
                            "Asimismo, GRUPO PROMOTOR DE OPERACIONES GPO no se hace responsable de la información contenida en la Aplicación, incluidas los elementos que se descarguen, en el entendido de que es bajo el propio riesgo y responsabilidad del Usuario el uso y seguimiento de la misma. Aunque se han realizado todos los esfuerzos para garantizar la seguridad de las actividades y operaciones de GRUPO PROMOTOR DE OPERACIONES GPO, se advierte a todos los Usuarios, que existen muchos métodos que los ciberdelincuentes usan para tratar de obtener datos personales para cometer fraudes. Por lo tanto, es responsabilidad del Usuario acatar todas las medidas para evitar los fraudes cibernéticos." +
                            "FECHA ACTUALIZACIÓN – 21/09/2022";


            var terminos = new List<Termino>() {
                new Termino() { Titulo = "I. DEFINICIONES.", Texto=tc1 },
                new Termino() { Titulo = "II.-	ACEPTACIÓN.", Texto=tc2 },
                new Termino() { Titulo = "III.- USO DE APLICACIÓN.", Texto=tc3 },
                new Termino() { Titulo = "IV.- REGISTRO", Texto = tc4 },
                new Termino() { Titulo = "V.- SERVICIOS DE LA APLICACIÓN", Texto=tc5 },
                new Termino() { Titulo = "VI.- COSTO DEL SERVICIO", Texto=tc6 },
                new Termino() { Titulo = "VII. CAMBIOS EN LA INFORMACIÓN", Texto = tc7 },
                new Termino() { Titulo = "VIII. DERECHOS DE AUTOR Y PROPIEDAD INDUSTRIAL", Texto = tc8 },
                new Termino() { Titulo = "IX. MODIFICACIONES A LA APLICACIÓN", Texto = tc9 },
                new Termino() { Titulo = "X. FALLAS EN EL SISTEMA", Texto = tc10 },
                new Termino() { Titulo = "XI. SEGURIDAD", Texto = tc11 },
                new Termino() { Titulo = "XII. MODIFICACIONES A LOS TÉRMINOS Y CONDICIONES", Texto = tc12 },
                new Termino() { Titulo = "XIII. AUTENTICACIÓN DE IDENTIDAD", Texto = tc13 },
                new Termino() { Titulo = "XIV. OBLIGACIONES LEGALES", Texto = tc14 },
                new Termino() { Titulo = "XV. DURACIÓN Y TERMINACIÓN", Texto = tc15},
                new Termino() { Titulo = "XVI. CUMPLIMIENTO REGULATORIO", Texto = tc16},
                new Termino() { Titulo = "XVII. LIMITANTE DE RESPONSABILIDAD", Texto = tc17 },
                new Termino() { Titulo = "XVIII. PAGOS A NOMBRE DE LA EMPRESA AFILIADA", Texto = tc18},
                new Termino() { Titulo = "XIX. CESIÓN", Texto = tc19 },
                new Termino() { Titulo = "XX. LEGISLACIÓN APLICABLE Y JURISDICCIÓN", Texto = tc20 },
                new Termino() { Titulo = "XXI. AVISO LEGAL", Texto = tc21 },
            };

            string ac1 = "En cumplimiento a lo establecido en la Ley Federal de Protección de Datos Personales en Posesión de los Particulares publicada en el Diario Oficial de la Federación(“DOF”) el 5 de julio de 2010(la “Ley”), su Reglamento publicado en el DOF el 21 de diciembre de 2011(el “Reglamento”) y los Lineamientos del Aviso de Privacidad publicados en el DOF el 17 de enero de 2013(los “Lineamientos” y conjuntamente con la Ley y el Reglamento, los “Ordenamientos”), GRUPO PROMOTOR DE OPERACIONES, S.A. DE C.V. (el “Responsable”) emite el presente aviso de privacidad (el “Aviso de Privacidad”) que contiene la política bajo la cual se recabarán y tratarán los datos personales(los “Datos Personales”) de las personas físicas(los “Titulares”) que en el apartado V se establecen.";

            string ac2 = "El objetivo del presente Aviso de Privacidad es delimitar los alcances y condiciones generales del tratamiento de los Datos Personales e informarlos a los Titulares, a fin de que estén en posibilidad de tomar decisiones informadas sobre el uso de sus Datos Personales y de mantener el control y disposición sobre ellos. Asimismo, el Aviso de Privacidad permite al responsable transparentar dicho tratamiento y con ello fortalecer el nivel de confianza de los Titulares.";
            
            string ac3 = "GRUPO PROMOTOR DE OPERACIONES, S.A. DE C.V. con domicilio en Calle Carlos R Ortiz No.37 Local 16 , Colonia Country Club, CP 83010, de la Ciudad de Hermosillo, Sonora, México (el “Domicilio para oír y recibir notificaciones”).";
            string ac4 = "Para el cumplimiento de este apartado, con fundamento en el artículo vigésimo segundo de los Lineamientos, el responsable cumplirá con su obligación de indicar los Datos Personales que se tratarán, identificando las categorías de los mismos." +
"Categorías de Datos Personales:" +
"a) Generales: Todos aquellos que permitan identificar al Titular." +
"b) De localización: Todos aquellos que permitan localizar al Titular, así como la localización de los bienes que, en su caso, se den en garantía." +
"c) Patrimoniales: Todos aquellos que permitan conocer la situación económica o financiera del Titular, así como las cuentas a su nombre vinculadas con el crédito que, en su caso, se celebre." +
"d) Laborales: Todos aquellos relacionados con su centro de trabajo." +
"Dentro de las categorías de Datos Personales no existen Datos Personales sensibles, entendiéndose por estos, aquellos que afecten a la esfera más íntima del Titular, o cuya utilización indebida pueda dar origen a discriminación o conlleve un riesgo grave para éste.";


            string ac5 = "El Responsable tratará los Datos Personales de los Titulares (incluyendo garantes y representantes) para las finalidades siguientes:" +
"A.	Finalidades que dan origen y son necesarias para la existencia, mantenimiento y cumplimiento de la relación jurídica entre el Responsable y el Titular se llegase a crear." +
"1.	Evaluar periódicamente la posibilidad de otorgarle un adelanto de nómina." +
"2.	Autentificar y corroborar la existencia del Titular, y la de la información proporcionada para el otorgamiento del préstamo y / o del adelanto de nómina." +
"3.	Analizar y / o verificar y/ o corroborar que el Titular tenga la capacidad y facultades suficientes para suscribir un contrato de préstamo con o sin garantía en calidad de acreditado." +
"4.	Analizar y / o verificar y/ o corroborar que el Titular cuenta con la solvencia y capacidad económica para cumplir con sus obligaciones de pago que resulten del crédito que, en su caso, se le otorgue." +
"5.	En su caso, la celebración del contrato de crédito o acto jurídico respectivo, así como la suscripción de sus anexos o instrumentos accesorios a dicha relación jurídica." +
"6.	Gestionar, en su caso, los registros y / o formalizaciones de los actos jurídicos celebrados." +
"7.	Poner a disposición los recursos del crédito, así como recibir los pagos por parte del Titular en términos del contrato respectivo." +
"8.	Exigir el cumplimiento de las obligaciones a cargo del Titular derivadas de los contratos que se suscriban." +
"9.	Modificar, en su caso, los contratos señalados." +
"10.	Mantener la comunicación que resulte necesaria para alcanzar el objeto de los contratos en los que conste la relación jurídica correspondiente." +
"11.	Ejercer las acciones judiciales o extrajudiciales que correspondan para hacer valer los derechos o hacer cumplir las obligaciones derivadas de la suscripción de los contratos correspondientes." +
"12.	Proporcionar información a terceros oferentes que, de cualquier forma, pretendan participar en el capital social del Responsable, o a oferentes en la adquisición de derechos y/ u obligaciones a favor o a cargo del propio Responsable." +
"13.	Ceder o transmitir a un tercero, mediante cualquier forma legal, los derechos y/ u obligaciones derivadas de los contratos antes señalados, según se establezca en los mismos." +
"14.	Confirmar que existe relación jurídica con el retenedor patrón e informarle del adelanto vis nomina que usted puede obtener." +
"15.	Confirmar que exista fuente de pago del préstamo." +
"B.	Finalidades que NO dan origen y NO son necesarias para la existencia, mantenimiento y cumplimiento de la relación jurídica entre el Responsable y el Titular." +
"1.	Generar y mantener el registro y bases de datos de acreditados." +
"2.	Elaborar encuestas, estadísticas y reportes." +
"3.	Notificar y ofrecer promociones, descuentos y campañas publicitarias." +
"En caso de que el Titular no desee que los Datos Personales sean tratados para todas o algunas de las finalidades enunciadas en el inciso B) anterior desde este momento podrá manifestarlo así mediante la presentación de un escrito libre dirigido a GRUPO PROMOTOR DE OPERACIONES, S.A. DE C.V. (“Persona Designada para la Protección de Datos Personales”), en el Domicilio para oír y recibir notificaciones, o bien por medio de un correo electrónico dirigido a la dirección siguiente: grupofinancierogpo@gmail.com.";

            string ac61 = "El tratamiento que el Responsable realiza de los Datos Personales puede involucrar la transferencia nacional o internacional de éstos. En virtud de lo anterior, a continuación se establecen los terceros receptores o destinatarios de los Datos Personales, indicando para ello, su tipo, categoría o sector de actividad. ";
            
            string ac62 = "Las finalidades que justifican la transferencia de Datos Personales son:" +
"Las previstas en los numerales 7, 14 y 15 del inciso A del apartado V." +
"Todo lo anterior, en el entendido de que el Responsable puede libremente hacer remisiones de los Datos Personales a sus prestadores de servicios para fines del propio Responsable, en virtud de que bajo los Ordenamientos estos terceros serían tan solo sus encargados.";
            
            string ac7 = "De conformidad con los Ordenamientos, el Titular, por su propio derecho o a través de su representante legal, podrá solicitarle al Responsable en cualquier momento el acceso, rectificación, cancelación u oposición(“Derechos ARCO”) respecto a los Datos Personales que le conciernen. 1.Acceso.Usted podrá verificar los Datos Personales con los que cuenta el Responsable, los cuales serán puestos a su disposición ya sea mediante copias físicas, documentos electrónicos o por cualquier otro medio que le indique el Responsable en términos de los Ordenamientos o que usted señale en su solicitud para ser contactado. 2.Rectificación.En caso de que el Responsable cuente con Datos Personales inexactos o incompletos, usted podrá solicitar la modificación total o parcial de los mismos, siempre y cuando presente el documento de soporte con el que acredite el error o el cambio en los mismos. 3.Oposición.Usted tendrá derecho en todo momento y por causa legítima conforme a la Ley, a oponerse al tratamiento parcial o total de sus Datos Personales recabados, pudiendo señalar casos o situaciones específicos para tal oposición. 4.Cancelación.Usted podrá solicitar a el Responsable que cancele el tratamiento de sus Datos Personales recabados en caso que considere que no se requieren para alguna de las finalidades señaladas en el presente Aviso de Privacidad o estén siendo utilizados para finalidades que no hayan sido consentidos.La solicitud deberá formularse mediante un escrito libre dirigido al área denominada “Gerencia Corporativa” y deberá contener y acompañar lo siguiente: (i)nombre del Titular y domicilio u otro medio para comunicarle la respuesta a su solicitud; (ii)copia del documento de identificación del Titular, así como el original de la misma para su cotejo o, en su caso, tratándose del representante del Titular, además de lo anterior(identificación del Titular), los documentos que acrediten la identidad del representante, así como el instrumento público o carta poder firmada ante dos testigos, en el que consten las facultades otorgadas, o declaración en comparecencia personal del Titular; (iii)descripción clara y precisa de los Datos Personales respecto de los que se busca ejercer alguno de los Derechos ARCO, y(iv) cualquier otro elemento o documento que facilite la localización de los Datos Personales.Dicha solicitud deberá presentarse en el Domicilio para oír y recibir notificaciones o mediante correo electrónico dirigido a la dirección siguiente: grupofinancierogpo@gmail.com. siempre que se presenten los instrumentos electrónicos debidamente certificados que sustituyan la identificación del Titular o, en su caso, de su representante.El ejercicio de los Derechos ARCO será gratuito, debiendo cubrir el Titular únicamente los gastos de envío, reproducción (copia simple) y, en su caso, certificación de documentos.No obstante, si el mismo Titular reitera su solicitud en un periodo menor a 12(doce) meses, los costos serán el equivalente a 3(tres) Unidades de Medida y Actualización, a menos que existan modificaciones sustanciales al Aviso de Privacidad que motiven nuevas consultas." +
"El Responsable comunicará al Titular en un plazo máximo de 20(veinte) días contados desde la fecha en que se haya recibido la solicitud del Derecho ARCO que corresponda, la determinación adoptada a efecto de que, si resulta procedente, se haga efectiva la misma dentro de los 15(quince) días siguientes a la fecha en que se comunique la respuesta.El plazo podrá ser ampliado por una sola vez por un periodo igual, siempre que el Responsable le justifique la ampliación al Titular, lo cual deberá ser notificado dentro del mismo plazo. Para tal efecto, el Responsable anotará en el acuse de recibo que entregue el Titular la correspondiente fecha de recepción." +
"El plazo señalado en el párrafo anterior, se interrumpirá en caso de que el Responsable requiera información adicional al Titular, en virtud de que la información entregada originalmente sea insuficiente o errónea para atenderla, o bien, no se acompañen los documentos antes indicados.Para tal efecto, el Responsable le podrá requerir al Titular, por una vez y dentro de los 5 (cinco) días siguientes a la recepción de la solicitud, aporte los elementos o documentos necesarios para dar trámite a la misma, contando, por su parte, el Titular con 10 (diez) días para atender el requerimiento, contados a partir del día siguiente en que lo haya recibido. Si el Titular no diere respuesta en dicho plazo se tendrá por no presentada la solicitud correspondiente. Por el contrario, en caso de que el Titular atienda el requerimiento de información, el plazo para que el Responsable dé respuesta a la solicitud, empezará a correr al día siguiente de que el Titular haya atendido el requerimiento de información adicional." +
"Las respuestas que el Responsable le otorgue a los Titulares que hubieren ejercido sus Derechos ARCO, se realizará por el mismo medio en que se formuló la solicitud, versando únicamente sobre los Datos Personales que específicamente se hayan indicado en la solicitud en cuestión y deberá presentarse en formato legible y comprensible." +
"Cuando el acceso a los Datos Personales sea en sitio, el Responsable le otorgará al Titular un periodo de 15(quince) días para que éste pueda presentarse a consultarlos. Transcurrido ese plazo, sin que el Titular haya acudido, será necesaria la presentación de una nueva solicitud." +
"Cuando el Responsable niegue el ejercicio de cualquiera de los derechos ARCO, deberá justificar su respuesta, informándole al Titular el derecho que le asiste para solicitar el inicio del procedimiento ante el Instituto Nacional de Transparencia, Acceso a la Información y Protección de Datos Personales.";

            var privacidad = new List<Termino>() {
                new Termino() { Titulo = "I. MARCO NORMATIVO.", Texto=ac1 },
                new Termino() { Titulo = "II. OBJETIVO.", Texto=ac2 },
                new Termino() { Titulo = "III. IDENTIDAD Y DOMICILIO DEL RESPONSABLE.", Texto=ac3 },
                new Termino() { Titulo = "IV. DATOS PERSONALES QUE SE TRATARÁN.", Texto=ac4 },
                new Termino() { Titulo = "V. FINALIDADES DEL TRATAMIENTO DE LOS DATOS PERSONALES. ", Texto=ac5 },
                new Termino() { Titulo = "A) Finalidades que dan origen y son necesarias para la existencia, mantenimiento y cumplimiento de la relación jurídica entre el Responsable y el Titular se llegase a crear.", Texto="" },
                new Termino() { Titulo = "1.", Texto="Evaluar periódicamente la posibilidad de otorgarle un adelanto de nómina. " },
                new Termino() { Titulo = "2.", Texto="Autentificar y corroborar la existencia del Titular, y la de la información proporcionada para el otorgamiento del préstamo y/o del adelanto de nómina. " },
                new Termino() { Titulo = "3.", Texto="Analizar y/o verificar y/o corroborar que el Titular tenga la capacidad y facultades suficientes para suscribir un contrato de préstamo con o sin garantía en calidad de acreditado. " },
                new Termino() { Titulo = "4.", Texto="Analizar y/o verificar y/o corroborar que el Titular cuenta con la solvencia y capacidad económica para cumplir con sus obligaciones de pago que resulten del crédito que, en su caso, se le otorgue." },
                new Termino() { Titulo = "5.", Texto="En su caso, la celebración del contrato de crédito o acto jurídico respectivo, así como la suscripción de sus anexos o instrumentos accesorios a dicha relación jurídica." },
                new Termino() { Titulo = "6.", Texto="Gestionar, en su caso, los registros y/o formalizaciones de los actos jurídicos celebrados." },
                new Termino() { Titulo = "7.", Texto="Poner a disposición los recursos del crédito, así como recibir los pagos por parte del Titular en términos del contrato respectivo." },
                new Termino() { Titulo = "8.", Texto="Exigir el cumplimiento de las obligaciones a cargo del Titular derivadas de los contratos que se suscriban." },
                new Termino() { Titulo = "9.", Texto="Modificar, en su caso, los contratos señalados." },
                new Termino() { Titulo = "10.", Texto="Mantener la comunicación que resulte necesaria para alcanzar el objeto de los contratos en los que conste la relación jurídica correspondiente." },
                new Termino() { Titulo = "11.", Texto="Ejercer las acciones judiciales o extrajudiciales que correspondan para hacer valer los derechos o hacer cumplir las obligaciones derivadas de la suscripción de los contratos correspondientes." },
                new Termino() { Titulo = "12.", Texto="Proporcionar información a terceros oferentes que, de cualquier forma, pretendan participar en el capital social del Responsable, o a oferentes en la adquisición de derechos y/u obligaciones a favor o a cargo del propio Responsable." },
                new Termino() { Titulo = "13.", Texto="Ceder o transmitir a un tercero, mediante cualquier forma legal, los derechos y/u obligaciones derivadas de los contratos antes señalados, según se establezca en los mismos." },
                new Termino() { Titulo = "14.", Texto="Confirmar que existe relación jurídica con el retenedor patrón e informarle del adelanto vis nomina que usted puede obtener." },
                new Termino() { Titulo = "15.", Texto="Confirmar que existe relación jurídica con el retenedor patrón e informarle del adelanto vis nomina que usted puede obtener." },
                new Termino() { Titulo = "B)	Finalidades que NO dan origen y NO son necesarias para la existencia, mantenimiento y cumplimiento de la relación jurídica entre el Responsable y el Titular.", Texto="" },
                new Termino() { Titulo = "1.", Texto="Generar y mantener el registro y bases de datos de acreditados." },
                new Termino() { Titulo = "2.", Texto="Elaborar encuestas, estadísticas y reportes." },
                new Termino() { Titulo = "3.", Texto="Notificar y ofrecer promociones, descuentos y campañas publicitarias." },
                new Termino() { Titulo = "VI. TRANSFERENCIAS DE DATOS PERSONALES.", Texto="" },
                new Termino() { Titulo = "VI.1. Terceros receptores o destinatarios.", Texto=ac61 },
                new Termino() { Titulo = "a)", Texto="Fedatarios Públicos." },
                new Termino() { Titulo = "b)", Texto="Oferentes en eventos de potenciales fusiones, escisiones o adquisiciones o en caso de ofertas accionarias u otros títulos." },
                new Termino() { Titulo = "c)", Texto="Oferentes u adquirentes de los derechos y/u obligaciones a cargo o a favor del Responsable y/o Titular" },
                new Termino() { Titulo = "d)", Texto="Empresas controladoras, filiales o subsidiarias del Responsable" },
                new Termino() { Titulo = "e)", Texto="Autoridades frente a las cuales el Responsable esté obligado en términos de ley." },
                new Termino() { Titulo = "VI.2. Finalidades que justifican transferencias de Datos Personales.", Texto=ac62 },
                new Termino() { Titulo = "VII. MEDIOS Y PROCEDIMIENTO PARA EL EJERCICIO DE LOS DERECHOS DE ACCESO, RECTIFICACIÓN CANCELACIÓN U OPOSICIÓN.", Texto=ac7 },
                new Termino() { Titulo = "VIII. REVOCACIÓN DEL CONSENTIMIENTO.", Texto="El consentimiento que el Titular otorgue al presente Aviso de Privacidad podrá ser revocado en cualquier tiempo, sin que se le atribuyan efectos retroactivos. La revocación del consentimiento que pretendiera realizar el Titular, deberá hacerlo conforme a los medios y procedimiento consignados en el numeral VII anterior." },
                new Termino() { Titulo = "IX. OPCIONES Y MEDIOS PARA LIMITAR EL USO O DIVULGACIÓN DE DATOS PERSONALES.", Texto="El Responsable informa a los Titulares de los Datos Personales, a través del presente Aviso de Privacidad que podrá, en su caso, limitar el uso y divulgación de sus Datos Personales, distintos al ejercicio de los Derechos ARCO o a la revocación del consentimiento, mediante la inscripción que decida realizar en el Registro Público de Usuarios a cargo de la Comisión Nacional para la Protección y Defensa de los Usuarios de Servicios Financieros." },
                new Termino() { Titulo = "X. CAMBIOS AL AVISO DE PRIVACIDAD.", Texto="El Responsable tiene el derecho de efectuar en cualquier momento modificaciones o actualizaciones al presente Aviso de Privacidad, con motivo de reformas legislativas, políticas internas o nuevos requerimientos para la prestación u ofrecimiento de nuestros servicios o productos. Estas modificaciones estarán disponibles a los Titulares a través de los medios siguientes: Anuncios visibles en nuestras oficinas o centros de atención a clientes; trípticos o folletos disponibles en nuestras oficinas o centros de atención a clientes, o se la haremos llegar al último correo electrónico que nos ha proporcionado." },
                new Termino() { Titulo = "", Texto="Para mayor información visite www.ifai.org.mx." },
            };

            if (Device.RuntimePlatform == Device.iOS)
            {
                terminos.Add(new Termino() { Titulo = "AVISO DE PRIVACIDAD INTEGRAL PARA CLIENTES", Texto = "" });
                foreach (var item in privacidad)
                {
                    terminos.Add(item);
                }
                this.Boardings = new ObservableCollection<Boarding>
                {
                    new Boarding()
                    {
                        ImagePath = "ReSchedule.png",
                        Header = "TÉRMINOS Y CONDICIONES",
                        SubHeader = "TÉRMINOS Y CONDICIONES DIRECTOS EN APP PARA USUARIOS",
                        Content = "",
                        HabilitarBoton = false,
                        TextoBoton = "Aceptar",
                        MostrarCheckBox = true,
                        TextoCheckBox = "ACEPTAR TÉRMINOS Y CONDICIONES",
                        ContentList = terminos,
                        RotatorView = new WalkthroughItemPage(this),
                    },
                };
            }
            else
            {
                this.Boardings = new ObservableCollection<Boarding>
                {
                    new Boarding()
                    {
                        ImagePath = "ReSchedule.png",
                        Header = "TÉRMINOS Y CONDICIONES",
                        SubHeader = "TÉRMINOS Y CONDICIONES DIRECTOS EN APP PARA USUARIOS",
                        Content = "",
                        HabilitarBoton = false,
                        TextoBoton = "Siguiente",
                        MostrarCheckBox = true,
                        TextoCheckBox = "ACEPTAR TÉRMINOS Y CONDICIONES",
                        ContentList = terminos,
                        RotatorView = new WalkthroughItemPage(this),
                    },
                    new Boarding()
                    {
                        ImagePath = "ViewMode.png",
                        Header = "AVISO DE PRIVACIDAD INTEGRAL PARA CLIENTES",
                        SubHeader = "",
                        Content = "",
                        HabilitarBoton = false,
                        TextoBoton = "Aceptar",
                        MostrarCheckBox = true,
                        TextoCheckBox = "ACEPTAR AVISO DE PRIVACIDAD",
                        ContentList = privacidad,
                        RotatorView = new WalkthroughItemPage(this),
                    },
                };
            }


            // Set bindingcontext to content view.
            foreach (var boarding in this.Boardings)
            {
                boarding.RotatorView.BindingContext = boarding;
            }
        }

        #endregion

        #region Properties

        public ObservableCollection<Boarding> Boardings
        {
            get
            {
                return this.boardings2;
            }

            private set
            {
                if (this.boardings2 == value)
                {
                    return;
                }

                this.SetProperty(ref this.boardings2, value);
            }
        }

        public string NextButtonText
        {
            get
            {
                return this.nextButtonText;
            }

            set
            {
                if (this.nextButtonText == value)
                {
                    return;
                }

                this.SetProperty(ref this.nextButtonText, value);
            }
        }

        public bool IsSkipButtonVisible
        {
            get
            {
                return this.isSkipButtonVisible;
            }

            set
            {
                if (this.isSkipButtonVisible == value)
                {
                    return;
                }

                this.SetProperty(ref this.isSkipButtonVisible, value);
            }
        }

        public int SelectedIndex
        {
            get
            {
                return _selectedIndex2;
            }

            set
            {
                if (_selectedIndex2 == value)
                {
                    return;
                }
                this.SetProperty(ref _selectedIndex2, value);
            }
        }

        #endregion

        #region Commands

        /// <summary>
        /// Gets or sets the command that is executed when the Skip button is clicked.
        /// </summary>
        public ICommand SalirCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the Done button is clicked.
        /// </summary>
        public ICommand NextCommand { get; set; }

        #endregion

        #region Methods

        private static void MoveToNextPage(MUsuario usuario)
        {
            Application.Current.MainPage.Navigation.PushModalAsync(new ValidateUserPage(usuario));
            //if (Device.RuntimePlatform == Device.UWP && Application.Current.MainPage.Navigation.NavigationStack.Count > 1)
            //{
            //    Application.Current.MainPage.Navigation.PopAsync();
            //}
            //else if (Device.RuntimePlatform != Device.UWP && Application.Current.MainPage.Navigation.NavigationStack.Count > 0)
            //{
            //    Application.Current.MainPage.Navigation.PopAsync();
            //}
        }

        private bool ValidateAndUpdateSelectedIndex(int itemCount)
        {
            if (this.SelectedIndex >= itemCount - 1)
            {
                return true;
            }

            this.SelectedIndex = _selectedIndex2 + 1;
            return false;
        }

        /// <summary>
        /// Invoked when the Skip button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private void Salir(object obj)
        {
            Application.Current.MainPage.Navigation.PopModalAsync();
        }

        /// <summary>
        /// Invoked when the Done button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private void Next(object obj)
        {
            if (this.ValidateAndUpdateSelectedIndex(this.Boardings.Count))
            {
                MoveToNextPage(this.Usuario);
            }
        }

        #endregion
    }
}
