﻿using FinnWorks.Entities;
using FinnWorks.Models;
using FinnWorks.Services.API;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace FinnWorks.ViewModels.LoginPage
{
    /// <summary>
    /// ViewModel for login page.
    /// </summary>
    [Preserve(AllMembers = true)]
    public class LoginPageViewModel : LoginViewModel
    {
        #region Fields

        private string password;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance for the <see cref="LoginPageViewModel" /> class.
        /// </summary>
        public LoginPageViewModel()
        {
            //this.Email = "test@adsistemas.mx";
            //this.Password = "finn_user_test12";

            this.LoginCommand = new Command(this.LoginClicked);
            this.SignUpCommand = new Command(this.SignUpClicked);
            this.ForgotPasswordCommand = new Command(this.ForgotPasswordClicked);
            this.SocialMediaLoginCommand = new Command(this.SocialLoggedIn);
        }

        #endregion

        #region property

        /// <summary>
        /// Gets or sets the property that is bound with an entry that gets the password from user in the login page.
        /// </summary>
        public string Password
        {
            get
            {
                return this.password;
            }

            set
            {
                if (this.password == value)
                {
                    return;
                }

                this.password = value;
                SetProperty(ref this.password, value);
            }
        }

        #endregion

        #region Command

        /// <summary>
        /// Gets or sets the command that is executed when the Log In button is clicked.
        /// </summary>
        public Command LoginCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the Sign Up button is clicked.
        /// </summary>
        public Command SignUpCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the Forgot Password button is clicked.
        /// </summary>
        public Command ForgotPasswordCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the social media login button is clicked.
        /// </summary>
        public Command SocialMediaLoginCommand { get; set; }

        #endregion

        #region methods

        /// <summary>
        /// Invoked when the Log In button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private async void LoginClicked(object obj)
        {
            if (IsBusy)
            {
                return;
            }

            string msg = "";
            IsBusy = true;
            MUsuario user = null;

            bool continuar = true;

            if (string.IsNullOrEmpty(this.Email) || this.Email.Trim().Length == 0)
            {
                continuar = false;
                msg = "El correo es requerido.";
            }

            if (continuar && (string.IsNullOrEmpty(this.Password) || this.Password.Trim().Length == 0))
            {
                continuar = false;
                msg = "La contraseña es requerida.";
            }

            if (continuar)
            {
                try
                {
                    var loginService = new UsuariosService();
                    user = await loginService.Login(Email, Password);

                    if (user == null)
                    {
                        msg = "El usuario/contraseña no son correctos.";
                    }
                }
                catch (System.Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                }
            }
              
            OnSesionIniciada(new LoginEventArgs(user, msg));
        }

        /// <summary>
        /// Invoked when the Sign Up button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private void SignUpClicked(object obj)
        {
            // Do something
        }

        /// <summary>
        /// Invoked when the Forgot Password button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private async void ForgotPasswordClicked(object obj)
        {
            var label = obj as Label;
            label.BackgroundColor = Color.FromHex("#70FFFFFF");
            await Task.Delay(100);
            label.BackgroundColor = Color.Transparent;
        }

        /// <summary>
        /// Invoked when social media login button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private void SocialLoggedIn(object obj)
        {
            // Do something
        }

        #endregion


        #region E V E N T O S
        public delegate void ResponseModelEventHandler(object sender, LoginEventArgs e);

        public event ResponseModelEventHandler SesionIniciada;
        protected virtual void OnSesionIniciada(LoginEventArgs e)
        {
            if (SesionIniciada != null)
                SesionIniciada(this, e);
        }
        #endregion
    }
}