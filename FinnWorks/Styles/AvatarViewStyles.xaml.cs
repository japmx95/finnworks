﻿using Xamarin.Forms.Xaml;

namespace FinnWorks.Styles
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AvatarViewStyles
    {
        public AvatarViewStyles()
        {
            this.InitializeComponent();
        }
    }
}