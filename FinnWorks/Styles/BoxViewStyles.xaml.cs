﻿using Xamarin.Forms.Xaml;

namespace FinnWorks.Styles
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BoxViewStyles
    {
        public BoxViewStyles()
        {
            this.InitializeComponent();
        }
    }
}