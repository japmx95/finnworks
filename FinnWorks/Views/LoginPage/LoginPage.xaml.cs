﻿using FinnWorks.ViewModels.LoginPage;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace FinnWorks.Views.LoginPage
{
    /// <summary>
    /// Page to login with user name and password
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage
    {
        public LoginPageViewModel ViewModel { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="LoginPage" /> class.
        /// </summary>
        public LoginPage()
        {
            InitializeComponent();
            this.BindingContext = ViewModel = new LoginPageViewModel();
            this.ViewModel.SesionIniciada += ViewModel_SesionIniciada;
        }

        private async void ViewModel_SesionIniciada(object sender, Models.LoginEventArgs e)
        {
            if (e.Usuario != null)
            {
                if (e.Usuario.Valido)
                {
                    if (!string.IsNullOrEmpty(e.Usuario.Personal))
                    {
                        await App.Database.SaveUsuario(e.Usuario);
                        if (e.Usuario.RangoDisposiciones != null)
                        {
                            await App.Database.SaveRangosDisposiciones(e.Usuario.RangoDisposiciones);
                        }
                        App.Current.MainPage = new AppShell();
                    }
                    else
                    {
                        await DisplayAlert("El usuario no tiene personal asignado.", "", "Aceptar");
                        ViewModel.IsBusy = false;
                        this.BindingContext = ViewModel;
                    }

                }
                else
                {
                    await Navigation.PushModalAsync(new TerminosPrivacidadPage(e.Usuario));
                    ViewModel.IsBusy = false;
                    this.BindingContext = ViewModel;
                }
            }
            else
            {
                await DisplayAlert(e.Mensaje, "", "Aceptar");
                ViewModel.IsBusy = false;
                this.BindingContext = ViewModel;
            }
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.IsBusy = false;
        }

        private void SfButton_Clicked(object sender, System.EventArgs e)
        {
            if (ViewModel.IsBusy)
            {
                return;
            }
            ViewModel.IsBusy = true;
            Navigation.PushModalAsync(new Registro.SignUpPage());           
        }
    }
}