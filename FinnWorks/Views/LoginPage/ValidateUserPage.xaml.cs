﻿using FinnWorks.Entities;
using FinnWorks.Services.API;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace FinnWorks.Views.LoginPage
{
    /// <summary>
    /// Page to sign in with user details.
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ValidateUserPage
    {
        public MUsuario Usuario { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="SignUpPage" /> class.
        /// </summary>
        public ValidateUserPage(MUsuario usuario)
        {
            InitializeComponent();
            this.Usuario = usuario;
        }


        private void BtnSalir_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        private async void BtnIniciarSesion_Clicked(object sender, System.EventArgs e)
        {
            if (Usuario != null)
            {
                var codigo = TxtCodigo.Text.Trim();
                var newPassword = TxtPassword.Text.Trim();
                var newPasswordConfirm = TxtConfirmPassword.Text.Trim();

                try
                {
                    if (newPassword != newPasswordConfirm)
                    {
                        await DisplayAlert("La contraseñas deben ser iguales", "", "Aceptar");
                        return;
                    }

                    var service = new UsuariosService();
                    var resultCodigo = await service.ValidarCodigo(Usuario, codigo, newPassword);

                    if (resultCodigo != null)
                    {
                        if (!resultCodigo.IsSuccess)
                        {
                            await DisplayAlert(resultCodigo.Message, "", "Aceptar");
                            return;
                        }
                        else
                        {
                            Usuario.Valido = true;
                            await App.Database.SaveUsuario(Usuario);
                            if (Usuario.RangoDisposiciones != null)
                            {
                                await App.Database.SaveRangosDisposiciones(Usuario.RangoDisposiciones);
                            }
                            App.Current.MainPage = new AppShell();
                        }
                    }
                    else
                    {
                        await DisplayAlert("No se pudo validar el código", "", "Aceptar");
                        return;
                    }
                }
                catch (System.Exception ex)
                {
                    await DisplayAlert(ex.ToString(), "Error", "Aceptar");
                    return;
                }

            }
        }
    }
}