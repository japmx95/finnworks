﻿using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;
using System;
using FinnWorks.ViewModels.InicioPage;
using System.Linq;
using System.Threading.Tasks;

namespace FinnWorks.Views.InicioPage
{
    /// <summary>
    /// Page to show the health profile.
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InicioPage : ContentPage
    {
        #region "P R O P I E D A D E S"
        public InicioPageViewModel ViewModel { get; set; }
        #endregion
        /// <summary>
        /// Initializes a new instance of the <see cref="InicioPage" /> class.
        /// </summary>
        public InicioPage()
        {
            InitializeComponent();

            this.BindingContext = ViewModel = new InicioPageViewModel();
            this.ViewModel.DisposicionSolicitada += ViewModel_DisposicionSolicitada;

            var hora = DateTime.Now.Hour;
            if (hora >= 6 && hora <= 11)
            {
                this.Title = "Buenos días!";
            }
            else if (hora >= 12 && hora <= 19)
            {
                this.Title = "Buenas tardes!";
            }
            else if ((hora >= 20 && hora <= 23) || (hora >= 0 && hora <= 5))
            {
                this.Title = "Buenas noches!";
            }

        }

        private void ViewModel_DisposicionSolicitada(object sender, Models.ResponseModelEventArgs e)
        {
            DisplayAlert(e.Response.IsSuccess ? "" : "Error", e.Response.Message, "Aceptar");
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.LoadItemsCommand.Execute(null);
            this.Content.Focus();
        }

        private async void IntegerEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            var entry = (Entry)sender;

            if (!String.IsNullOrEmpty(e.NewTextValue))
            {
                if (decimal.TryParse(e.NewTextValue, out decimal parsedDecimal))
                {
                    if (parsedDecimal <= ViewModel.SaldoDisponible)
                    {
                        entry.Text = parsedDecimal.ToString();
                    }
                    else
                    {
                        if (parsedDecimal > 0)
                        {
                            entry.Text = ViewModel.SaldoDisponible.ToString();
                        }
                        else
                        {
                            entry.Text = "0";
                        }
                    }
                }
                decimal montoDisponer = Convert.ToDecimal(entry.Text);

                await CalcularComision(montoDisponer);
            }
        }

        private async void amount_ValueChanged(object sender, Syncfusion.SfNumericUpDown.XForms.ValueEventArgs e)
        {   
            decimal montoDisponer = Convert.ToDecimal(e.Value.ToString());

            await CalcularComision(montoDisponer);
        }

        private async Task CalcularComision(decimal montoDisponer) {
            System.Collections.Generic.List<Entities.MRangoDisposicion> rangoEmpresa = await App.Database.GetRangoDisposiciones();

            if (montoDisponer >= 1)
            {
                decimal porcentajeIVA = 0.16m;
                Entities.MRangoDisposicion rango = rangoEmpresa.FirstOrDefault(x => montoDisponer >= x.RangoMinimo && montoDisponer <= x.RangoMaximo);
                if (rango == null)
                {
                    decimal maxComison = rangoEmpresa.Max(x => x.Comision);
                    rango = rangoEmpresa.FirstOrDefault(x => x.Comision == maxComison);
                }

                if (rango != null)
                {
                    decimal comision = rango.Comision / (1 + porcentajeIVA);
                    decimal iva = Math.Round(comision * porcentajeIVA, 2);

                    decimal total = Math.Round(rango.Comision + ViewModel.ImporteDisposicion,2);

                    LblComision.Text = string.Format(" {0:C}", comision);
                    LblIVA.Text = string.Format(" {0:C}", iva);
                    LblTotal.Text = string.Format(" {0:C}", total);
                }
            }
            else
            {
                LblComision.Text = string.Format(" {0:C}", 0);
                LblIVA.Text = string.Format(" {0:C}", 0);
                LblTotal.Text = string.Format(" {0:C}", 0);
            }
        }
    }
}