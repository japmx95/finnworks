﻿using FinnWorks.Services.API;
using FinnWorks.ViewModels.Registro;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace FinnWorks.Views.Registro
{
    /// <summary>
    /// Page to sign in with user details.
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignUpPage
    {
        public SignUpPageViewModel ViewModel { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="SignUpPage" /> class.
        /// </summary>
        public SignUpPage()
        {
            InitializeComponent();
            this.BindingContext = ViewModel = new SignUpPageViewModel();
            this.ViewModel.UsuarioSolicitado += ViewModel_UsuarioSolicitado;
        }

        private void ViewModel_UsuarioSolicitado(object sender, Models.ResponseModelEventArgs e)
        {
            if (e.Response.IsSuccess)
            {
                DisplayAlert("", e.Response.Message, "aceptar");
                ViewModel.IsBusy = false;
                this.BindingContext = ViewModel;
            }
            else
            {
                DisplayAlert("Error", e.Response.Message, "aceptar");
                ViewModel.IsBusy = false;
                this.BindingContext = ViewModel;
            }
        }

        private void BtnSalir_Clicked(object sender, System.EventArgs e)
        {
            ViewModel.IsBusy = false;
            Navigation.PopModalAsync();
        }

    }
}