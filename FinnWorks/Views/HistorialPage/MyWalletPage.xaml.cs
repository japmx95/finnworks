﻿using FinnWorks.ViewModels.HistorialPage;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace FinnWorks.Views.HistorialPage
{
    /// <summary>
    /// My wallet page.
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyWalletPage : ContentPage
    {
        #region "P R O P I E D A D E S"
        public MyWalletViewModel ViewModel { get; set; }
        #endregion
        /// <summary>
        /// Initializes a new instance of the <see cref="MyWalletPage"/> class.
        /// </summary>
        public MyWalletPage()
        {
            InitializeComponent();
            this.BindingContext = ViewModel = new MyWalletViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.LoadItemsCommand.Execute(null);
        }
    }
}