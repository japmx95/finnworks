﻿using FinnWorks.Models;
using FinnWorks.ViewModels;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace FinnWorks.Views
{
    /// <summary>
    /// Page to display on-boarding gradient with animation
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WalkthroughItemPage
    {
        public OnBoardingAnimationViewModel BoardingViewModel { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="WalkthroughItemPage" /> class.
        /// </summary>
        public WalkthroughItemPage(OnBoardingAnimationViewModel BoardingViewModel)
        {
            this.InitializeComponent();
            this.BoardingViewModel = BoardingViewModel;
        }

        private void nextButton_Clicked(object sender, System.EventArgs e)
        {
            this.BoardingViewModel.NextCommand.Execute(null);
        }

        private void SfCheckBox_StateChanged(object sender, Syncfusion.XForms.Buttons.StateChangedEventArgs e)
        {
            var bindingContext = this.BindingContext as Boarding;
            bindingContext.HabilitarBoton = e.IsChecked.Value;

            this.BindingContext = bindingContext;
            nextButton.IsEnabled = bindingContext.HabilitarBoton;
        }
    }
}