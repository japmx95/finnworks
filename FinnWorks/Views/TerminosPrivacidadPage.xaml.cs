﻿using FinnWorks.Entities;
using FinnWorks.ViewModels;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace FinnWorks.Views
{
    /// <summary>
    /// Page to display on-boarding gradient with animation
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TerminosPrivacidadPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TerminosPrivacidadPage" /> class.
        /// </summary>
        public TerminosPrivacidadPage(MUsuario usuario)
        {
            this.InitializeComponent();
            this.BindingContext = new OnBoardingAnimationViewModel(usuario);
        }
    }
}