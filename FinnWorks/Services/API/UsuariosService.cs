﻿using FinnWorks.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using FinnWorks.Models;

namespace FinnWorks.Services.API
{
    public class UsuariosService : BaseAPIService
    {
        public async Task<MUsuario> Login(string user, string pass)
        {
            MUsuario retorno = null;

            var parametros = new JObject()
            {
                new JProperty("user",user),
                new JProperty("pass",pass)
            };

            var response = await PostRequest(new AuthRequestModel() { Params = parametros.ToString() }, "usuarios/auth");

            if (response.Result != null)
            {
                retorno = JsonConvert.DeserializeObject<MUsuario>(response.Result.ToString());
            }

            return retorno;
        }

        public async Task<ResponseModel> Registrar(string email, string nombre, string password, string curp)
        {
            var data = new JObject(
                                    new JProperty("mail", email),
                                    new JProperty("name", nombre),
                                    new JProperty("pass", password),
                                    new JProperty("curp", curp)
                                   );

            return await PostRequest(new AuthRequestModel() { Params = data.ToString() }, "usuarios/registro");
        }

        public async Task<ResponseModel> ValidarCodigo(MUsuario usuario, string codigo, string newPassword)
        {
            var data = new JObject(new JProperty("codigo", codigo), new JProperty("password", newPassword));

            return await PostRequest(new RequestModel(new DataModel(usuario.ApiKey, data)), "usuarios/validarcodigo");
        }
    }
}
