﻿using FinnWorks.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using FinnWorks.Models;
using FinnWorks.Models.Disposiciones;
using System.Collections.Generic;

namespace FinnWorks.Services.API
{
    public class DisposicionesService : BaseAPIService
    {
        public async Task<EstadoCuenta> GetEstadoCuenta(MUsuario usuario)
        {
            EstadoCuenta retorno = null;

            var response = await PostRequest(new RequestModel(new DataModel(usuario.ApiKey, null)), "disposiciones/edocuenta");

            if (response.Result != null)
            {
                retorno = JsonConvert.DeserializeObject<EstadoCuenta>(response.Result.ToString());
            }

            return retorno;
        }

        public async Task<List<MovimientoHistorial>> GetHistorial(MUsuario usuario)
        {
            List<MovimientoHistorial> retorno = null;

            var response = await PostRequest(new RequestModel(new DataModel(usuario.ApiKey, null)), "disposiciones/historial");

            if (response.Result != null)
            {
                retorno = JsonConvert.DeserializeObject<List<MovimientoHistorial>>(response.Result.ToString());
            }

            return retorno;
        }

        public async Task<ResponseModel> SolicitarDisposicion(MUsuario usuario, decimal ImporteSolicitado)
        {
            var data = new JObject(new JProperty("ImporteSolicitado", ImporteSolicitado));

            return await PostRequest(new RequestModel(new DataModel(usuario.ApiKey, data)), "disposiciones/solicitar");
        }
    }
}