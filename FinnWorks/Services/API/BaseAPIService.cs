﻿using FinnWorks.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace FinnWorks.Services.API
{
    public class BaseAPIService
    {
        const string URLBase = "http://sig.adssistemas.mx:86/api/";
        //const string URLBase = "http://192.168.1.65:8081/api/";
        public const string URLBasePerfiles = "http://sig.adssistemas.mx:86/Personal/ImagenesPerfil/";

        protected async Task<ResponseModel> PostRequest<T>(T model, string Metodo)
        {
            ResponseModel resp = null;
            
            using (var cliente = new HttpClient())
            {
                cliente.Timeout = new TimeSpan(0, 0, 10);
                cliente.DefaultRequestHeaders.Add("Accept", "application/json");
                try
                {
                    string content = "";
                    var json = JsonConvert.SerializeObject(model);

                    var response = await cliente.PostAsync(URLBase + Metodo, new StringContent(json, Encoding.UTF8, "application/json"));

                    content = await response.Content.ReadAsStringAsync();

                    resp = JsonConvert.DeserializeObject<ResponseModel>(content);
                }
                catch (Exception ex)
                {
                    resp = new ResponseModel() { IsSuccess=false,Message=ex.ToString(),Result = null };
                }


            }
            return resp;
        }


    }
}
