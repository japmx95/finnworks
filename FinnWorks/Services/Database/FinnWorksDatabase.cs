﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinnWorks.Helpers;
using FinnWorks.Entities;

namespace FinnWorks.Services.Database
{
    public class FinnWorksDatabase
    {
        static readonly Lazy<SQLiteAsyncConnection> lazyInitializer = new Lazy<SQLiteAsyncConnection>(() =>
        {
            return new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
        });

        static SQLiteAsyncConnection Database => lazyInitializer.Value;
        static bool initialized = false;

        public FinnWorksDatabase()
        {
            InitializeAsync().SafeFireAndForget(false);
        }

        async Task InitializeAsync()
        {
            if (!initialized)
            {
                if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(MUsuario).Name))
                {
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(MUsuario)).ConfigureAwait(false);
                }

                if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(MRangoDisposicion).Name))
                {
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(MRangoDisposicion)).ConfigureAwait(false);
                }
                initialized = true;
            }
        }

        #region "U S U A R I O S"

        public Task<MUsuario> GetUsuarioActual()
        {
            var user = Database.Table<MUsuario>().FirstOrDefaultAsync();
            return user;
        }

        public async Task<int> SaveUsuario(MUsuario item)
        {
            await Database.DeleteAllAsync<MUsuario>();

            var inserto = await Database.InsertAsync(item);
            return inserto;
        }

        public Task<List<MRangoDisposicion>> GetRangoDisposiciones()
        {         
            return Database.Table<MRangoDisposicion>().ToListAsync();
        }

        public async Task<int> SaveRangosDisposiciones(List<MRangoDisposicion> items)
        {
            await Database.DeleteAllAsync<MRangoDisposicion>();

            var inserto = await Database.InsertAllAsync(items);
            return inserto;
        }

        public Task<int> DeleteAllUsers()
        {
            return Database.DeleteAllAsync<MUsuario>();
        }
        public Task<int> DeleteAllRangos()
        {
            return Database.DeleteAllAsync<MRangoDisposicion>();
        }
        #endregion
    }
}
