using FinnWorks.Services.Database;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

//using Microsoft.AppCenter;
//using Microsoft.AppCenter.Analytics;
//using Microsoft.AppCenter.Crashes;

[assembly: ExportFont("Montserrat-Bold.ttf",Alias="Montserrat-Bold")]
     [assembly: ExportFont("Montserrat-Medium.ttf", Alias = "Montserrat-Medium")]
     [assembly: ExportFont("Montserrat-Regular.ttf", Alias = "Montserrat-Regular")]
     [assembly: ExportFont("Montserrat-SemiBold.ttf", Alias = "Montserrat-SemiBold")]
     [assembly: ExportFont("UIFontIcons.ttf", Alias = "FontIcons")]
namespace FinnWorks
{
    public partial class App : Application
    {
        private string SyncfusionLicense = "NDA1MjEzQDMxMzgyZTM0MmUzMEZNMFp2REduT1RGSVNCTnVLVy9yc2VhMkxscGRjT3NYUXFMZzBzRTc5S009";
        public static string BaseImageUrl { get; } = "https://cdn.syncfusion.com/essential-ui-kit-for-xamarin.forms/common/uikitimages/";
        private static FinnWorksDatabase database;
        public static FinnWorksDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new FinnWorksDatabase();
                }
                return database;
            }
        }

        public App()
        {
            //AppCenter.Start("ios=15dfe1b8-41cf-4bfb-9856-f11f02f45f20;"
            //      //+"android={Your Android App secret here}"
            //      , typeof(Analytics), typeof(Crashes));

            //Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MzYzNDIyQDMxMzgyZTMzMmUzME1EWjhkZDQ1OEU3MVpzYkx5eG4xOE1rZVdsLzhCRlh4eTZXR2NLRE92Rlk9");
            //Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("NjM4MTAxQDMyMzAyZTMxMmUzME9pdDVTZDcya01hSlpwSHo0OHVHZmtoVWZOUHE0cE9IUXpIOFByOXBYOFU9");
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense(SyncfusionLicense);
            InitializeComponent();
            MainPage = new Views.LoginPage.LoginPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
