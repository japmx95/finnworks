﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinnWorks.Entities
{
    public class MUsuario
    {
        public string Usuario { get; set; }
        public string Nombre { get; set; }
        public string Personal { get; set; }
        public string ApiKey { get; set; }
        public string ImagenPerfil { get; set; }

        public bool Valido { get; set; }

        public string CodigoAcceso { get; set; }

        [SQLite.Ignore]
        public List<MRangoDisposicion> RangoDisposiciones { get; set; }
    }

    public class MRangoDisposicion
    {
        public decimal RangoMinimo { get; set; }
        public decimal RangoMaximo { get; set; }
        public decimal Comision { get; set; }
    }
}
