﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinnWorks.Models
{
    public class RequestModel 
    {
        public RequestModel() 
        {
        
        }

        public RequestModel(DataModel Params)
        {
            this.Params = Params;
        }

        public DataModel Params { get; set; }
    }
    public class DataModel
    {
        public DataModel(string ApiKey, object Data) 
        {
            this.ApiKey = ApiKey;
            this.Data = Data;
        }
        public string ApiKey { get; set; }
        public object Data { get; set; }
    }
}
