﻿namespace FinnWorks.Models.Disposiciones
{
    public class EstadoCuenta
    {
        public decimal ImporteSolicitado { get; set; }
        public decimal ImportePagado { get; set; }
        public decimal ImporteDisponible { get; set; }
        public decimal ImporteRechazado { get; set; }
    }
}
