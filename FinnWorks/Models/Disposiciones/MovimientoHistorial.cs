﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinnWorks.Models.Disposiciones
{
    public class MovimientoHistorial
    {
        public int CveDisposicion { get; set; }
        public DateTime FechaDisposicion { get; set; }
        public int? CveConcepto { get; set; }
        public string Concepto { get; set; }
        public DateTime? FechaMovimiento { get; set; }
        public int? CveMovimiento { get; set; }
        public double Importe { get; set; }
    }
}
