﻿using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace FinnWorks.Models
{
    /// <summary>
    /// Model for OnBoarding
    /// </summary>
    [Preserve(AllMembers = true)]
    public class Boarding
    {
        #region Properties

        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        public string ImagePath { get; set; }

        /// <summary>
        /// Gets or sets the header.
        /// </summary>
        public string Header { get; set; }

        public string SubHeader { get; set; }

        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        public string Content { get; set; }
        public bool HabilitarBoton { get; set; }
        public string TextoBoton { get; set; }
        public bool MostrarCheckBox { get; set; }
        public string TextoCheckBox { get; set; }

        public List<Termino> ContentList { get; set; }

        /// <summary>
        /// Gets or sets the view.
        /// </summary>
        public View RotatorView { get; set; }

        #endregion
    }
    /// <summary>
    /// Model for OnBoarding
    /// </summary>
    [Preserve(AllMembers = true)]
    public class Termino
    {
        #region Properties
        /// <summary>
        /// Gets or sets the Titulo.
        /// </summary>
        /// 

        public Termino() 
        {
            this.Punto = "&#xe720;";
        }
        public string Punto { get; set; }
        public string Titulo { get; set; }

        /// <summary>
        /// Gets or sets the Texto.
        /// </summary>
        public string Texto { get; set; }

        #endregion
    }
}
