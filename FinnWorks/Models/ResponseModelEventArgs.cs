﻿using FinnWorks.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinnWorks.Models
{
    public class ResponseModelEventArgs : EventArgs
    {
        public ResponseModelEventArgs(ResponseModel Response) : base()
        {
            this.Response = Response;
        }
        public ResponseModel Response { get; set; }
    }

    public class LoginEventArgs : EventArgs
    {
        public LoginEventArgs(MUsuario usuario,string Mensaje  = "") : base()
        {
            this.Usuario = usuario;
            this.Mensaje = Mensaje;
        }
        public MUsuario Usuario { get; set; }

        public string Mensaje { get; set; }
    }
}
